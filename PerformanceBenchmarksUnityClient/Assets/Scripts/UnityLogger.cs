﻿using System;
using CoreCommon;
using CoreCommon.DI.Attributes;

[CoreRegister(true)]
public sealed class UnityLogger : ICoreLogger
{
  private string GetColoredMessage(LogType logType, ref string message)
  {
    switch (logType)
    {
      case LogType.Debug:
        return $"<color=white>{message}</color>";
      case LogType.Info:
        return $"<color=blue>{message}</color>";
      case LogType.Warning:
        return $"<color=yellow>{message}</color>";
      default:
        return $"<color=red>{message}</color>";
    }
  }

  public void Log(string log, LogType logType) => UnityEngine.Debug.Log(GetColoredMessage(logType, ref log));
}