﻿using System.Threading.Tasks;
using CoreClient;
using CoreCommon;
using CoreCommon.DI;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 649

namespace Assets.Scripts
{
  public class SceneClientWorker : MonoBehaviour
  {
    [SerializeField] private GameObject _startButtonsGrid;
    [SerializeField] private Button _startTestButton;
    [SerializeField] private Button _startSyncTestButton;
    [SerializeField] private Button _stopTestButton;
    private AsyncClientsRunner _asyncClientsRunner;
    private ClientsRunner _clientsRunner;

    private void Awake()
    {
      //God.WorldCreation(God.RegistrationType.Mobile);
      SerializableTypesInitializer.Register();
      _startSyncTestButton.onClick.AddListener(StartAsyncButton);  
      _startTestButton.onClick.AddListener(StartButton);
      _stopTestButton.onClick.AddListener(StopButton);
    }

    private void StartAsyncButton()
    {
      _asyncClientsRunner = God.PrayFor<AsyncClientsRunner>();
      Task.Run(async () =>
      {     
        await _asyncClientsRunner.Run("http://www.space-smuggler.com/").ConfigureAwait(false);
      });
      _startButtonsGrid.SetActive(false);
      _stopTestButton.gameObject.SetActive(true);
      _stopTestButton.interactable = true;
    }

    private void StartButton()
    {
      _clientsRunner = God.PrayFor<ClientsRunner>();
      Task.Run(async () =>
      {
        await _clientsRunner.Run("http://www.space-smuggler.com/").ConfigureAwait(false);
      });
      _startButtonsGrid.SetActive(false);
      _stopTestButton.gameObject.SetActive(true);
      _stopTestButton.interactable = true;
    }

    private void StopButton()
    {
      _asyncClientsRunner?.Dispose();
      _clientsRunner?.Dispose();
      _startButtonsGrid.SetActive(true);
      _stopTestButton.interactable = false;
      _stopTestButton.gameObject.SetActive(false);
    }
  }
}
