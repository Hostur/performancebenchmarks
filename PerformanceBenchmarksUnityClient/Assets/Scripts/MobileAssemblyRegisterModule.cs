﻿using Autofac;
using CoreCommon;
using CoreCommon.DI;

namespace Assets.Scripts
{
  public class MobileAssemblyRegisterModule : IRegisterAssemblyModule
  {
    public void Register(ContainerBuilder builder)
    {
      builder.Register(c => new UnityLogger())
        .As<UnityLogger>()
        .As<ICoreLogger>()
        .Keyed<object>(typeof(UnityLogger).FullName)
        .SingleInstance();
    }

    public int RegistrationOrder => 0;
  }
}
