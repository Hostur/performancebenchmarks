﻿using System;
using System.Collections.Generic;
using CoreCommon.Serializable;
using ZeroFormatter.Comparers;
using ZeroFormatter.Formatters;
using ZeroFormatter.Internal;

namespace CoreCommon
{
  public static class SerializableTypesInitializer
  {
    private static bool _registered = false;

    public static void Register()
    {
      try
      {
        if (_registered) return;
        _registered = true;
        // Enums
        Formatter<DefaultResolver, Serializable.ConfirmationState>.Register(
          new ConfirmationStateFormatter<DefaultResolver>());
        ZeroFormatterEqualityComparer<Serializable.ConfirmationState>.Register(new ConfirmationStateEqualityComparer());
        Formatter<DefaultResolver, Serializable.ConfirmationState?>.Register(
          new NullableConfirmationStateFormatter<DefaultResolver>());
        ZeroFormatterEqualityComparer<Serializable.ConfirmationState?>.Register(
          new NullableEqualityComparer<Serializable.ConfirmationState>());

        // Objects
        // Structs
        {
          var structFormatter = new NetworkRequestFormatter<DefaultResolver>();
          Formatter<DefaultResolver, Serializable.NetworkRequest>.Register(structFormatter);
          Formatter<DefaultResolver, Serializable.NetworkRequest?>.Register(
            new NullableStructFormatter<DefaultResolver, Serializable.NetworkRequest>(structFormatter));
        }
        {
          var structFormatter = new ConfirmationRequestFormatter<DefaultResolver>();
          Formatter<DefaultResolver, Serializable.ConfirmationRequest>.Register(structFormatter);
          Formatter<DefaultResolver, Serializable.ConfirmationRequest?>.Register(
            new NullableStructFormatter<DefaultResolver, Serializable.ConfirmationRequest>(structFormatter));
        }
        {
          var structFormatter = new RTTRequestFormatter<DefaultResolver>();
          Formatter<DefaultResolver, Serializable.RTTRequest>.Register(structFormatter);
          Formatter<DefaultResolver, Serializable.RTTRequest?>.Register(
            new NullableStructFormatter<DefaultResolver, Serializable.RTTRequest>(structFormatter));
        }
        {
          var structFormatter = new SerializableVector3Formatter<DefaultResolver>();
          Formatter<DefaultResolver, Serializable.SerializableVector3>.Register(structFormatter);
          Formatter<DefaultResolver, Serializable.SerializableVector3?>.Register(
            new NullableStructFormatter<DefaultResolver, Serializable.SerializableVector3>(structFormatter));
        }
        {
          var structFormatter = new Vector3RequestFormatter<DefaultResolver>();
          Formatter<DefaultResolver, Serializable.Vector3Request>.Register(structFormatter);
          Formatter<DefaultResolver, Serializable.Vector3Request?>.Register(
            new NullableStructFormatter<DefaultResolver, Serializable.Vector3Request>(structFormatter));
        }
        // Unions
        // Generics
        Formatter.RegisterArray<DefaultResolver, NetworkRequest>();

        Console.WriteLine("Structures registered.");
      }
      catch (Exception e)
      {
        throw new Exception("Zero formatter registration failed." + e);
      }
    }

    public class NetworkRequestFormatter<TTypeResolver> : Formatter<TTypeResolver, Serializable.NetworkRequest>
      where TTypeResolver : ITypeResolver, new()
    {
      readonly Formatter<TTypeResolver, byte> formatter0;
      readonly Formatter<TTypeResolver, byte[]> formatter1;

      public override bool NoUseDirtyTracker
      {
        get
        {
          return formatter0.NoUseDirtyTracker
                 && formatter1.NoUseDirtyTracker
            ;
        }
      }

      public NetworkRequestFormatter()
      {
        formatter0 = Formatter<TTypeResolver, byte>.Default;
        formatter1 = Formatter<TTypeResolver, byte[]>.Default;

      }

      public override int? GetLength()
      {
        return null;
      }

      public override int Serialize(ref byte[] bytes, int offset, Serializable.NetworkRequest value)
      {
        var startOffset = offset;
        offset += formatter0.Serialize(ref bytes, offset, value.Id);
        offset += formatter1.Serialize(ref bytes, offset, value.Data);
        return offset - startOffset;
      }

      public override Serializable.NetworkRequest Deserialize(ref byte[] bytes, int offset,
        ZeroFormatter.DirtyTracker tracker, out int byteSize)
      {
        byteSize = 0;
        int size;
        var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;
        var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;

        return new Serializable.NetworkRequest(item0, item1);
      }
    }


    public class
      ConfirmationRequestFormatter<TTypeResolver> : Formatter<TTypeResolver, Serializable.ConfirmationRequest>
      where TTypeResolver : ITypeResolver, new()
    {
      readonly Formatter<TTypeResolver, int> formatter0;
      readonly Formatter<TTypeResolver, int> formatter1;
      readonly Formatter<TTypeResolver, int> formatter2;
      readonly Formatter<TTypeResolver, string> formatter3;

      public override bool NoUseDirtyTracker
      {
        get
        {
          return formatter0.NoUseDirtyTracker
                 && formatter1.NoUseDirtyTracker
                 && formatter2.NoUseDirtyTracker
                 && formatter3.NoUseDirtyTracker
            ;
        }
      }

      public ConfirmationRequestFormatter()
      {
        formatter0 = Formatter<TTypeResolver, int>.Default;
        formatter1 = Formatter<TTypeResolver, int>.Default;
        formatter2 = Formatter<TTypeResolver, int>.Default;
        formatter3 = Formatter<TTypeResolver, string>.Default;

      }

      public override int? GetLength()
      {
        return null;
      }

      public override int Serialize(ref byte[] bytes, int offset, Serializable.ConfirmationRequest value)
      {
        var startOffset = offset;
        offset += formatter0.Serialize(ref bytes, offset, value.ConfirmationStateValue);
        offset += formatter1.Serialize(ref bytes, offset, value.UdpPort);
        offset += formatter2.Serialize(ref bytes, offset, value.NetworkId);
        offset += formatter3.Serialize(ref bytes, offset, value.AuthorizationToken);
        return offset - startOffset;
      }

      public override Serializable.ConfirmationRequest Deserialize(ref byte[] bytes, int offset,
        ZeroFormatter.DirtyTracker tracker, out int byteSize)
      {
        byteSize = 0;
        int size;
        var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;
        var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;
        var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;
        var item3 = formatter3.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;

        return new Serializable.ConfirmationRequest(item0, item1, item2, item3);
      }
    }

    public class RTTRequestFormatter<TTypeResolver> : Formatter<TTypeResolver, Serializable.RTTRequest>
      where TTypeResolver : ITypeResolver, new()
    {
      readonly Formatter<TTypeResolver, DateTime> formatter0;

      public override bool NoUseDirtyTracker
      {
        get
        {
          return formatter0.NoUseDirtyTracker
            ;
        }
      }

      public RTTRequestFormatter()
      {
        formatter0 = Formatter<TTypeResolver, DateTime>.Default;

      }

      public override int? GetLength()
      {
        return 12;
      }

      public override int Serialize(ref byte[] bytes, int offset, Serializable.RTTRequest value)
      {
        BinaryUtil.EnsureCapacity(ref bytes, offset, 12);
        var startOffset = offset;
        offset += formatter0.Serialize(ref bytes, offset, value.ServerSendTime);
        return offset - startOffset;
      }

      public override Serializable.RTTRequest Deserialize(ref byte[] bytes, int offset,
        ZeroFormatter.DirtyTracker tracker, out int byteSize)
      {
        byteSize = 0;
        int size;
        var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;

        return new Serializable.RTTRequest(item0);
      }
    }

    public class
      SerializableVector3Formatter<TTypeResolver> : Formatter<TTypeResolver, Serializable.SerializableVector3>
      where TTypeResolver : ITypeResolver, new()
    {
      readonly Formatter<TTypeResolver, float> formatter0;
      readonly Formatter<TTypeResolver, float> formatter1;
      readonly Formatter<TTypeResolver, float> formatter2;

      public override bool NoUseDirtyTracker
      {
        get
        {
          return formatter0.NoUseDirtyTracker
                 && formatter1.NoUseDirtyTracker
                 && formatter2.NoUseDirtyTracker
            ;
        }
      }

      public SerializableVector3Formatter()
      {
        formatter0 = Formatter<TTypeResolver, float>.Default;
        formatter1 = Formatter<TTypeResolver, float>.Default;
        formatter2 = Formatter<TTypeResolver, float>.Default;

      }

      public override int? GetLength()
      {
        return 12;
      }

      public override int Serialize(ref byte[] bytes, int offset, Serializable.SerializableVector3 value)
      {
        BinaryUtil.EnsureCapacity(ref bytes, offset, 12);
        var startOffset = offset;
        offset += formatter0.Serialize(ref bytes, offset, value.X);
        offset += formatter1.Serialize(ref bytes, offset, value.Y);
        offset += formatter2.Serialize(ref bytes, offset, value.Z);
        return offset - startOffset;
      }

      public override Serializable.SerializableVector3 Deserialize(ref byte[] bytes, int offset,
        ZeroFormatter.DirtyTracker tracker, out int byteSize)
      {
        byteSize = 0;
        int size;
        var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;
        var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;
        var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;

        return new Serializable.SerializableVector3(item0, item1, item2);
      }
    }

    public class Vector3RequestFormatter<TTypeResolver> : Formatter<TTypeResolver, Serializable.Vector3Request>
      where TTypeResolver : ITypeResolver, new()
    {
      readonly Formatter<TTypeResolver, Serializable.SerializableVector3> formatter0;

      public override bool NoUseDirtyTracker
      {
        get
        {
          return formatter0.NoUseDirtyTracker
            ;
        }
      }

      public Vector3RequestFormatter()
      {
        formatter0 = Formatter<TTypeResolver, Serializable.SerializableVector3>.Default;

      }

      public override int? GetLength()
      {
        return null;
      }

      public override int Serialize(ref byte[] bytes, int offset, Serializable.Vector3Request value)
      {
        var startOffset = offset;
        offset += formatter0.Serialize(ref bytes, offset, value.Vector);
        return offset - startOffset;
      }

      public override Serializable.Vector3Request Deserialize(ref byte[] bytes, int offset,
        ZeroFormatter.DirtyTracker tracker, out int byteSize)
      {
        byteSize = 0;
        int size;
        var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
        offset += size;
        byteSize += size;

        return new Serializable.Vector3Request(item0);
      }
    }

    public class ConfirmationStateFormatter<TTypeResolver> : Formatter<TTypeResolver, Serializable.ConfirmationState>
      where TTypeResolver : ITypeResolver, new()
    {
      public override int? GetLength()
      {
        return 4;
      }

      public override int Serialize(ref byte[] bytes, int offset, Serializable.ConfirmationState value)
      {
        return BinaryUtil.WriteInt32(ref bytes, offset, (Int32) value);
      }

      public override Serializable.ConfirmationState Deserialize(ref byte[] bytes, int offset,
        ZeroFormatter.DirtyTracker tracker, out int byteSize)
      {
        byteSize = 4;
        return (Serializable.ConfirmationState) BinaryUtil.ReadInt32(ref bytes, offset);
      }
    }


    public class
      NullableConfirmationStateFormatter<TTypeResolver> : Formatter<TTypeResolver, Serializable.ConfirmationState?>
      where TTypeResolver : ITypeResolver, new()
    {
      public override int? GetLength()
      {
        return 5;
      }

      public override int Serialize(ref byte[] bytes, int offset, Serializable.ConfirmationState? value)
      {
        BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
        if (value.HasValue)
        {
          BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32) value.Value);
        }
        else
        {
          BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
        }

        return 5;
      }

      public override Serializable.ConfirmationState? Deserialize(ref byte[] bytes, int offset,
        ZeroFormatter.DirtyTracker tracker, out int byteSize)
      {
        byteSize = 5;
        var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
        if (!hasValue) return null;

        return (Serializable.ConfirmationState) BinaryUtil.ReadInt32(ref bytes, offset + 1);
      }
    }



    public class ConfirmationStateEqualityComparer : IEqualityComparer<Serializable.ConfirmationState>
    {
      public bool Equals(Serializable.ConfirmationState x, Serializable.ConfirmationState y)
      {
        return (Int32) x == (Int32) y;
      }

      public int GetHashCode(Serializable.ConfirmationState x)
      {
        return (int) x;
      }
    }
  }
}
