﻿namespace CoreCommon.DI
{
  /// <summary>
  /// Use this interface to inject custom registration to <see cref="God"/>.
  /// If you inject custom registration it cause changing default standalone registration into the mobile (hard-coded) one.
  /// </summary>
  public interface IRegisterAssemblyModule
  {
    /// <summary>
    /// Negative values before default.
    /// Positive values after default.
    /// For example loggers and settings should be registered as low as possible with negative values.
    /// Each default values are registered on -100 so each order above that value will override default registration.
    /// </summary>
    int RegistrationOrder { get; }
    void Register(Autofac.ContainerBuilder builder);
  }
}
