﻿using System.Collections.Generic;
using CoreCommon.Serializable;

namespace CoreCommon
{
  public struct NetworkRequestsQueue
  {
    private readonly Queue<NetworkRequest> _networkRequests;

    public NetworkRequestsQueue(int capacity)
    {
      _networkRequests = new Queue<NetworkRequest>(capacity);
    }

    public void Enqueue(ref NetworkRequest request)
    {
      lock(_networkRequests)
        _networkRequests.Enqueue(request);
    }

    public bool TryDequeue(out NetworkRequest[] requests)
    {
      lock (_networkRequests)
      {
        if (_networkRequests.Count < 1)
        {
          requests = null;
          return false;
        }
        else
        {
          requests = _networkRequests.ToArray();
          _networkRequests.Clear();
          return true;
        }
      }
    }
  }
}
