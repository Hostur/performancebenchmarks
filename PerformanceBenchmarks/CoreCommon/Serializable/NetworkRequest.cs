﻿using ZeroFormatter;

namespace CoreCommon.Serializable
{
  [ZeroFormattable]
  public struct NetworkRequest
  { 
    [Index(0)] public readonly byte Id;
    [Index(1)] public readonly byte[] Data;

    public NetworkRequest(byte id, byte[] data)
    {
      Id = id;
      Data = data;
    }
  }
}
