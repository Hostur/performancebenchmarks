﻿using System;
using System.Runtime.CompilerServices;
using ZeroFormatter;

namespace CoreCommon.Serializable
{
  [Serializable]
  [ZeroFormattable]
  public struct SerializableVector3
  {
    [Index(0)]
    public float X;
    [Index(1)]
    public float Y;
    [Index(2)]
    public float Z;

    public SerializableVector3(float x, float y, float z)
    {
      X = x;
      Y = y;
      Z = z;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public SerializableVector3 Normalized()
    {
      float magnitude = (float)Math.Sqrt(X * X + Y * Y + Z * Z);
      return new SerializableVector3(X / magnitude, Y / magnitude, Z / magnitude);
    }

    #region Operators
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static SerializableVector3 operator -(SerializableVector3 vec1, SerializableVector3 vec2)
    {
      return new SerializableVector3(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static SerializableVector3 operator +(SerializableVector3 vec1, SerializableVector3 vec2)
    {
      return new SerializableVector3(vec1.X + vec2.X, vec1.Y + vec2.Y, vec1.Z + vec2.Z);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static SerializableVector3 operator *(SerializableVector3 vec1, SerializableVector3 vec2)
    {
      return new SerializableVector3(vec1.X * vec2.X, vec1.Y * vec2.Y, vec1.Z * vec2.Z);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static SerializableVector3 operator *(SerializableVector3 vec1, float value)
    {
      return new SerializableVector3(vec1.X * value, vec1.Y * value, vec1.Z * value);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static SerializableVector3 operator /(SerializableVector3 vec1, float value)
    {
      if (value == 0)
      {
        throw new DivideByZeroException();
      }
      return new SerializableVector3(vec1.X / value, vec1.Y / value, vec1.Z / value);
    }
    #endregion
  }
}
