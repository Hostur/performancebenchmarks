﻿using ZeroFormatter;

namespace CoreCommon.Serializable
{
  /// <summary>
  /// Identifier = 2.
  /// </summary>
  [ZeroFormattable]
  public struct Vector3Request
  {
    public const byte REQUEST_IDENTIFIER = 2;

    [Index(0)] public SerializableVector3 Vector;
    public Vector3Request(SerializableVector3 vector) => Vector = vector;
  }
}
