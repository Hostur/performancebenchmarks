﻿using ZeroFormatter;

namespace CoreCommon.Serializable
{
  public enum ConfirmationState
  {
    RequestConfirmation = 0,
    Confirmation = 1,
    ConfirmationAccepted = 2,
    ConfirmationFailed = 3
  }

  /// <summary>
  /// Request identifier = 0.
  /// This request handling handshake with the server.
  /// </summary>
  [ZeroFormattable]
  [Preserve(AllMembers = true)]
  public struct ConfirmationRequest
  {
    /// <summary>
    /// Serialization constructor for ZeroFormatter.
    /// </summary>
    public ConfirmationRequest(int confirmationStateValue, int udpPort, int networkId, string authorizationToken)
    {
      ConfirmationStateValue = confirmationStateValue;
      UdpPort = udpPort;
      NetworkId = networkId;
      AuthorizationToken = authorizationToken;
    }

    /// <summary>
    /// Request identifier.
    /// </summary>
    public const byte REQUEST_IDENTIFIER = 0;
    [IgnoreFormat] public ConfirmationState ConfirmationState => (ConfirmationState)ConfirmationStateValue;

     [Index(0)] public int ConfirmationStateValue;
     [Index(1)] public int UdpPort;
     [Index(2)] public int NetworkId;
     [Index(3)] public string AuthorizationToken;

    public ConfirmationRequest(ConfirmationState confirmationState)
    {
      ConfirmationStateValue = (int) confirmationState;
      AuthorizationToken = string.Empty;
      UdpPort = -1;
      NetworkId = -1;
    }

    public ConfirmationRequest(string authorizationToken)
    {
      ConfirmationStateValue = (int)ConfirmationState.Confirmation;
      AuthorizationToken = authorizationToken;
      UdpPort = -1;
      NetworkId = -1;
    }

    public ConfirmationRequest(int udpPort, int networkId)
    {
      ConfirmationStateValue = (int)ConfirmationState.ConfirmationAccepted;
      AuthorizationToken = string.Empty;
      UdpPort = udpPort;
      NetworkId = networkId;
    }
  }
}
