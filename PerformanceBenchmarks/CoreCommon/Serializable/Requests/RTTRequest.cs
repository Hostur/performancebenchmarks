﻿using System;
using ZeroFormatter;

namespace CoreCommon.Serializable
{
  /// <summary>
  /// Request identifier = 1.
  /// </summary>
  [ZeroFormattable]

  public struct RTTRequest
  {
    public const byte REQUEST_IDENTIFIER = 1;

    [Index(0)]
    public DateTime ServerSendTime;

    public RTTRequest(DateTime dateTime) => ServerSendTime = dateTime;
  }
}
