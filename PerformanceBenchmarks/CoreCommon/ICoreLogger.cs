﻿namespace CoreCommon
{
  public enum LogType
  {
    Debug,
    Info,
    Warning,
    Error
  }

  public interface ICoreLogger
  {
    void Log(string log, LogType logType);
  }
}
