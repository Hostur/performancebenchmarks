﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoreCommon.SharedUtils
{
  public abstract class AsyncHttpProvider
  {
    private readonly int _requestsTimeout;
    private readonly string _webServiceUrl;

    protected AsyncHttpProvider(string webServiceUrl, int requestsTimeout)
    {
      _requestsTimeout = requestsTimeout;
      _webServiceUrl = webServiceUrl;
    }

    protected async Task<WebServiceResponse<T>> GetAsync<T>(string url)
    {
      using (HttpClient client = new HttpClient())
      {
        CancellationTokenSource source = new CancellationTokenSource(_requestsTimeout);
        using (HttpResponseMessage response = await client.GetAsync(_webServiceUrl + url, source.Token).ConfigureAwait(false))
        {
          if (response.IsSuccessStatusCode)
          {
            using (var content = response.Content)
            {
              var stringContent = await content.ReadAsStringAsync().ConfigureAwait(false);
              return new WebServiceResponse<T>(JsonConvert.DeserializeObject<T>(stringContent));
            }
          }

          response.Content?.Dispose();
          return ToWebServiceResponse<T>(response);
        }
      }
    }

    protected async Task<WebServiceResponse<string>> GetAsyncString(string url)
    {
      using (HttpClient client = new HttpClient())
      {
        CancellationTokenSource source = new CancellationTokenSource(_requestsTimeout);
        using (HttpResponseMessage response = await client.GetAsync(_webServiceUrl + url, source.Token).ConfigureAwait(false))
        {
          if (response.IsSuccessStatusCode)
          {
            using (var content = response.Content)
            {
              var stringContent = await content.ReadAsStringAsync().ConfigureAwait(false);
              return new WebServiceResponse<string>(stringContent);
            }
          }

          response.Content?.Dispose();
          return ToWebServiceResponse<string>(response);
        }
      }
    }

    protected async Task<WebServiceResponse<T>> PostAsync<T, B>(string url, B body)
    {
      using (HttpClient client = new HttpClient())
      {
        var jsonObject = JsonConvert.SerializeObject(body);
        var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");
        CancellationTokenSource source = new CancellationTokenSource(_requestsTimeout);
        using (var response = await client.PostAsync(_webServiceUrl + url, content, source.Token))
        {
          if (response.IsSuccessStatusCode)
          {
            using (var responseContent = response.Content)
            {
              string responseBody = await responseContent.ReadAsStringAsync().ConfigureAwait(false);
              return new WebServiceResponse<T>(JsonConvert.DeserializeObject<T>(responseBody));
            }
          }

          response.Content?.Dispose();
          return ToWebServiceResponse<T>(response);
        }
      }
    }

    protected async Task<WebServiceResponse<bool>> PostAsync<B>(string url, B body)
    {
      using (HttpClient client = new HttpClient())
      {
        var jsonObject = JsonConvert.SerializeObject(body);
        var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");
        CancellationTokenSource source = new CancellationTokenSource(_requestsTimeout);
        using (var response = await client.PostAsync(_webServiceUrl + url, content, source.Token))
        {
          return new WebServiceResponse<bool>(response.IsSuccessStatusCode);
        }
      }
    }
    private WebServiceResponse<T> ToWebServiceResponse<T>(HttpResponseMessage message)
    {
      switch (message.StatusCode)
      {
        case HttpStatusCode.BadRequest:
          return new WebServiceResponse<T>(400);

        default:
          return new WebServiceResponse<T>(404);
      }
    }
  }
}
