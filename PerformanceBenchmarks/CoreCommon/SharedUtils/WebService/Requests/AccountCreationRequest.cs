﻿using CoreCommon.SharedUtils.Types;

namespace CoreCommon.SharedUtils.Requests
{
  public class AccountCreationRequest
  {
    public LoginType LoginType;
    public string Token;
    public string Email;
    public string Nickname;
    public int Avatar;

    public AccountCreationRequest(LoginType loginType, string token, string email, string nickname, int avatar)
    {
      LoginType = loginType;
      Token = token;
      Email = email;
      Nickname = nickname;
      Avatar = avatar;
    }

    public AccountCreationRequest() { }
  }
}
