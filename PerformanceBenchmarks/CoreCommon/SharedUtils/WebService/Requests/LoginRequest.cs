﻿using CoreCommon.SharedUtils.Types;

namespace CoreCommon.SharedUtils.Requests
{
  public class LoginRequest
  {
    public LoginType LoginType { get; set; }

    /// <summary>
    /// Encrypted user guid with login type.
    /// </summary>
    public string AuthorizationToken { get; set; }

    public LoginRequest(string authorizationToken, LoginType loginType)
    {
      AuthorizationToken = authorizationToken;
      LoginType = loginType;
    }

    public LoginRequest() {}
  }
}
