﻿using CoreCommon.SharedUtils.Types;

namespace CoreCommon.SharedUtils.Requests
{
  public struct PublishServerStateRequest
  {
    public string ServerAuthorizationToken;
    public ServerState State;
    public WorldType WorldType;
    public string LocalIpAddress;
    public int Port;

    public PublishServerStateRequest(string serverAuthorizationToken, ServerState state, WorldType worldType, string localIpAddress, string publicIpAddress, int port)
    {
      ServerAuthorizationToken = serverAuthorizationToken;
      State = state;
      WorldType = worldType;
      LocalIpAddress = localIpAddress;
      Port = port;
    }
  }

  public enum ServerState
  {
    Available,
    NonAvailable,
    Close
  }
}