﻿using CoreCommon.SharedUtils.Types;

namespace CoreCommon.SharedUtils.Requests
{
  public class ValidateUserRequest
  {
    public string AuthorizationToken { get; set; }
    public WorldType WorldType { get; set; }

    public ValidateUserRequest(string authorizationToken, WorldType worldType)
    {
      AuthorizationToken = authorizationToken;
      WorldType = worldType;
    }
  }
}
