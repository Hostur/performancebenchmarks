﻿namespace CoreCommon.SharedUtils.Requests
{
  public class ClientAuthorizationRequest
  {
    public string AuthorizationToken { get; set; }
    public ClientAuthorizationRequest(string authorizationToken) => AuthorizationToken = authorizationToken;
  }
}
