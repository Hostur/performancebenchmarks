﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using CoreCommon.SharedUtils.Requests;
using CoreCommon.SharedUtils.Responses;
using CoreCommon.SharedUtils.RSA;
using CoreCommon.SharedUtils.Types;

namespace CoreCommon.SharedUtils
{
  public sealed class WebServiceProvider : AsyncHttpProvider
  {
    private const int REQUESTS_TIMEOUT = 8000;
    private const string BASE_SERVICE_URL = "http://www.space-smuggler.com/";

    private const string LOGIN = "game/login";
    private const string GET_PUBLIC_RSA = "api/get_public_rsa";
    private const string GET_BEST_SERVER_INFO = "api/servers/best";
    private const string GET_SERVERS_INFO = "api/servers";
    private const string GET_PRODUCTS = "api/products";
    private const string VALIDATE_USER = "server/validate/user";
    private const string PUBLISH_SERVER_STATE = "api/servers/publish";
    private const string ACCOUNT_CREATE = "account/create";

    public bool Initialized => _rsaClient != null;
    private RsaClient _rsaClient;

    public WebServiceProvider(bool autoInitialize, string webServiceUrl = BASE_SERVICE_URL, int requestsTimeout = REQUESTS_TIMEOUT) : base(webServiceUrl, requestsTimeout)
    {
      ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
      if (autoInitialize)
        Task.Run(async () => await Initialize().ConfigureAwait(false)).ConfigureAwait(false);
    }
    public async Task Initialize() => _rsaClient = await InternalInitialize().ConfigureAwait(false);

    //public ClientAuthorizationRequest GetClientAuthorizationRequest(string authorizationToken)
    //{
    //  if (!Initialized)
    //    throw new Exception("WebServiceProvider not initialized.");

    //  return new ClientAuthorizationRequest(_rsaClient.Encrypt(authorizationToken));
    //}

    public LoginRequest GetLoginRequest(LoginType loginType, string authorizationToken)
    {
      if (!Initialized)
        throw new Exception("WebServiceProvider not initialized.");

      return new LoginRequest(_rsaClient.Encrypt(authorizationToken), loginType);
    }

    public async Task<WebServiceResponse<LoginResponse>> Login(LoginRequest request)
    {
      return await PostAsync<LoginResponse, LoginRequest>(LOGIN, request).ConfigureAwait(false);
    }

    public async Task<WebServiceResponse<ServerInfo>> GetServer()
    {
      return await GetAsync<ServerInfo>(GET_BEST_SERVER_INFO).ConfigureAwait(false);
    }

    public async Task<WebServiceResponse<List<ServerInfo>>> GetServers()
    {
      return await GetAsync<List<ServerInfo>>(GET_SERVERS_INFO).ConfigureAwait(false);
    }

    public async Task<WebServiceResponse<bool>> PublishServer(PublishServerStateRequest request)
    {
      return await PostAsync<PublishServerStateRequest>(PUBLISH_SERVER_STATE, request).ConfigureAwait(false);
    }

    public async Task<WebServiceResponse<UserValidationResponse>> ValidateUser(ValidateUserRequest request)
    {
      return await PostAsync<UserValidationResponse, ValidateUserRequest>(VALIDATE_USER, request).ConfigureAwait(false);
    }

    public async Task<WebServiceResponse<AccountCreationResponse>> CreateAccount(AccountCreationRequest request)
    {
      return await PostAsync<AccountCreationResponse, AccountCreationRequest>(ACCOUNT_CREATE, request).ConfigureAwait(false);
    }

    private async Task<RsaClient> InternalInitialize()
    {
      var publicKey = await GetAsyncString(GET_PUBLIC_RSA).ConfigureAwait(false);
      return (publicKey.IsSuccessful) ? new RsaClient(publicKey.Value) : null;
    }

    public bool MyRemoteCertificateValidationCallback(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
      bool isOk = true;
      // If there are errors in the certificate chain, look at each error to determine the cause.
      if (sslPolicyErrors != SslPolicyErrors.None)
      {
        for (int i = 0; i < chain.ChainStatus.Length; i++)
        {
          if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
          {
            chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
            chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
            chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
            chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
            bool chainIsValid = chain.Build((X509Certificate2)certificate);
            if (!chainIsValid)
            {
              isOk = false;
            }
          }
        }
      }
      return isOk;
    }
  }
}
