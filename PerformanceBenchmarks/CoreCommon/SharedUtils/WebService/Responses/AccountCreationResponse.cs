﻿namespace CoreCommon.SharedUtils.Responses
{
  public enum AccountCreationResponseType
  {
    UnknownError,
    NicknameAlreadyInUse,
    InvalidNickname,
    NicknameForbidden,
    NotValidEmail,
    EmailAlreadyInUse,
    Ok
  }
  public class AccountCreationResponse
  {
    public AccountCreationResponseType AccountCreationResponseType;
    public AccountCreationResponse(AccountCreationResponseType type) => AccountCreationResponseType = type;
    public AccountCreationResponse() {}
  }
}
