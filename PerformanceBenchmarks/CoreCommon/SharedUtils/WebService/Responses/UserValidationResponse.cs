﻿namespace CoreCommon.SharedUtils.Responses
{
  public class UserValidationResponse
  {
    public int NetworkId { get; set; }
    public string Nickname { get; set; }

    public UserValidationResponse(int networkId, string nickname)
    {
      NetworkId = networkId;
      Nickname = nickname;
    }

    public UserValidationResponse() {}
  }
}
