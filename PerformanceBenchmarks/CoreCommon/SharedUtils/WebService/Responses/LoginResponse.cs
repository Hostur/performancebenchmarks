﻿using CoreCommon.SharedUtils.Types;

namespace CoreCommon.SharedUtils.Responses
{
  public class LoginResponse
  {
    public bool IsEmailConfirmed { get; set; }
    public bool FirstLoginToday { get; set; }
    public int LoggedInRow { get; set; }
    public UserInfo UserInfo { get; set; }

    public LoginResponse(bool firstLoginToday, int loggedInRow, UserInfo userInfo)
    {
      FirstLoginToday = firstLoginToday;
      LoggedInRow = loggedInRow;
      UserInfo = userInfo;
      IsEmailConfirmed = true;
    }

    public LoginResponse() { IsEmailConfirmed = false; }
  }
}
