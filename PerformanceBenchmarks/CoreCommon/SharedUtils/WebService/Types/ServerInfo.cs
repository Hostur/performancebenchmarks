﻿
namespace CoreCommon.SharedUtils.Types
{
  public class ServerInfo
  {
    public ServerInfo(){}
    public string Address { get; set; }
    public string PrivateAddress { get; set; }
    public int Port { get; set; }
    public bool Available { get; set; }
    public ContinentCode ContinentCode { get; set; }

    public WorldType WorldType { get; set; }
  }

  public enum ContinentCode
  {
    UNDEFINED,
    EU, //Europe
    OC, //Oceania
    NA, //North America
    AF, //Africa
    AS, //Asia
    SA, //South America
    AN  //Antarctica
  }
}
