﻿namespace CoreCommon.SharedUtils.Types
{
  /// <summary>
  /// This wold type is used in database to store position in relation to specific type of world.
  /// Different game servers may exists with specific type of wold. Free account have access only for a YoungGalaxy.
  /// </summary>
  public enum WorldType
  {
    YoungGalaxy,
    OldGalaxy,
    AncientGalaxy
  }
}
