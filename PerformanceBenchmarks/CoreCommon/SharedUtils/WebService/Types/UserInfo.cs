﻿namespace CoreCommon.SharedUtils.Types
{
  public class UserInfo
  {
    /// <summary>
    /// Unique network id.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Unique nickname.
    /// </summary>
    public string Nickname { get; set; }

    public UserInfo(int id, string nickname)
    {
      Id = id;
      Nickname = nickname;
    }

    public UserInfo() {}
  }
}
