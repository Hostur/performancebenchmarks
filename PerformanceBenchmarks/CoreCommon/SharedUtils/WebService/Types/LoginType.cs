﻿namespace CoreCommon.SharedUtils.Types
{
  /// <summary>
  /// Type of account that is used for log in.
  /// </summary>
  public enum LoginType
  {
    /// <summary>
    ///  Google.
    /// </summary>
    G,

    /// <summary>
    /// iOS.
    /// </summary>
    I,

    /// <summary>
    /// Facebook.
    /// </summary>
    F,

    /// <summary>
    /// Steam.
    /// </summary>
    S,

    /// <summary>
    /// Custom
    /// </summary>
    C
  }
}
