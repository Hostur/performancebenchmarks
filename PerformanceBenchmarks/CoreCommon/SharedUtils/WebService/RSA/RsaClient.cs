﻿namespace CoreCommon.SharedUtils.RSA
{
  public class RsaClient : RsaBase
  {
    private readonly string _publicKey;

    public RsaClient(string publicKey)
    {
      _publicKey = publicKey;
    }

    public string Encrypt(string content)
    {
      return Encrypt(content, _publicKey);
    }
  }
}