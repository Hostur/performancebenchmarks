﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace CoreCommon.SharedUtils.RSA
{
  public abstract class RsaBase
  {
    protected string Encrypt(string textToEncrypt, string publicKeyString)
    {
      var bytesToEncrypt = Encoding.UTF8.GetBytes(textToEncrypt);

      using (var rsa = new RSACryptoServiceProvider(2048))
      {
        try
        {
          FromXmlStringExt(rsa, publicKeyString);
          var encryptedData = rsa.Encrypt(bytesToEncrypt, true);
          var base64Encrypted = Convert.ToBase64String(encryptedData);
          return base64Encrypted;
        }
        finally
        {
          rsa.PersistKeyInCsp = false;
        }
      }
    }

    protected string Decrypt(string textToDecrypt, string privateKeyString)
    {
      using (var rsa = new RSACryptoServiceProvider(2048))
      {
        try
        {
          FromXmlStringExt(rsa, privateKeyString);

          var resultBytes = Convert.FromBase64String(textToDecrypt);
          var decryptedBytes = rsa.Decrypt(resultBytes, true);
          var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
          return decryptedData;
        }
        finally
        {
          rsa.PersistKeyInCsp = false;
        }
      }
    }

    public void FromXmlStringExt(System.Security.Cryptography.RSA rsa, string xmlString)
    {
      var parameters = new RSAParameters();

      var xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(xmlString);

      if (xmlDoc.DocumentElement.Name.Equals("RSAKeyValue"))
      {
        foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
        {
          switch (node.Name)
          {
            case "Modulus":
              parameters.Modulus =
                string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "Exponent":
              parameters.Exponent =
                string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "P":
              parameters.P = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "Q":
              parameters.Q = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "DP":
              parameters.DP = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "DQ":
              parameters.DQ = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "InverseQ":
              parameters.InverseQ =
                string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "D":
              parameters.D = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
          }
        }
      }
      else
      {
        throw new Exception("Invalid XML RSA key.");
      }

      rsa.ImportParameters(parameters);
    }
  }
}
