﻿//--------------------------------
// 200 - Ok
// 400 - Bad request
// 404 - Invalid url
// 777 - Invalid login or password
// 778 - Internal error
//--------------------------------
namespace CoreCommon.SharedUtils
{
  public class WebServiceResponse<T>
  {
    public int ResponseCode { get; }
    public T Value { get; }

    public WebServiceResponse(T value)
    {
      ResponseCode = 200;
      Value = value;
    }

    public WebServiceResponse(T value, int responseCode)
    {
      ResponseCode = responseCode;
      Value = value;
    }

    public WebServiceResponse(int responseCode)
    {
      ResponseCode = responseCode;
    }

    public bool IsSuccessful => ResponseCode == 200;
  }
}