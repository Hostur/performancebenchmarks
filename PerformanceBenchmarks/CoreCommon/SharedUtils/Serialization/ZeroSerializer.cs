﻿#define ZERO_FORMATTER

using System.Runtime.CompilerServices;
#if ZERO_FORMATTER

namespace CoreCommon.SharedUtils.Serialization
{
  public static partial class ZeroSerializer
  {
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static byte[] Serialize<T>(this T data) where T : struct => ZeroFormatter.ZeroFormatterSerializer.Serialize(data);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static byte[] SerializeArray<T>(this T[] data) where T : struct => ZeroFormatter.ZeroFormatterSerializer.Serialize(data);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T Deserialize<T>(this byte[] data) where T : struct => ZeroFormatter.ZeroFormatterSerializer.Deserialize<T>(data);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T[] DeserializeArray<T>(this byte[] data) where T : struct => ZeroFormatter.ZeroFormatterSerializer.Deserialize<T[]>(data);
  }
}
#endif