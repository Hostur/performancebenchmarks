﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoreCommon.SharedUtils
{
  public static class IP
  {
    public static async Task<string> GetPublicIpAddress()
    {
      string externalip = await new WebClient().DownloadStringTaskAsync("http://icanhazip.com").ConfigureAwait(true);
      return Regex.Replace(externalip, @"\t|\n|\r", "");
    }

    public static string GetLocalIPAddress()
    {
      var host = Dns.GetHostEntry(Dns.GetHostName());
      foreach (var ip in host.AddressList)
      {
        if (ip.AddressFamily == AddressFamily.InterNetwork)
        {
          return ip.ToString();
        }
      }

      Console.WriteLine("No network adapters with an IPv4 address in the system!");
      throw new Exception();
    }
  }
}
