﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace CoreCommon.SharedUtils
{
  //internal static class ManualResetEventPool
  //{
  //  private static Queue<ManualResetEvent> _events = new Queue<ManualResetEvent>(2048);

  //  public static ManualResetEvent GetEvent()
  //  {
  //    lock (_events)
  //    {
  //      if (_events.Count > 0)
  //        return _events.Dequeue();
  //    }
  //    return new ManualResetEvent(false);
  //  }

  //  public static void Accomplish(this ManualResetEvent e)
  //  {
  //    e.Set();
  //    e.Reset();
  //    lock (_events)
  //    {
  //      _events.Enqueue(e);
  //    }
  //  }
  //}

  public static partial class SocketExtension
  {
    private const int PREFIX = 4;

    public static void ReceiveFromTcp(this Socket client, out byte[] result)
    {
      byte[] receivePrefix = new byte[PREFIX];
      int receiveTotal = 0;

      int receive = client.Receive(receivePrefix, 0, 4, 0);
      int receiveSize = BitConverter.ToInt32(receivePrefix, 0);
      int receiveDataLeft = receiveSize;

      result = new byte[receiveSize];
      while (receiveTotal < receiveSize)
      {
        receive = client.Receive(result, receiveTotal, receiveDataLeft, 0);
        if (receive == 0)
        {
          break;
        }
        receiveTotal += receive;
        receiveDataLeft -= receive;
      }
    }

    public static byte[] ReceiveFromTcp(this Socket client)
    {
      byte[] receivePrefix = new byte[PREFIX];
      int receiveTotal = 0;

      int receive = client.Receive(receivePrefix, 0, 4, 0);
      int receiveSize = BitConverter.ToInt32(receivePrefix, 0);
      int receiveDataLeft = receiveSize;

      byte[] data = new byte[receiveSize];
      while (receiveTotal < receiveSize)
      {
        receive = client.Receive(data, receiveTotal, receiveDataLeft, 0);
        if (receive == 0)
        {
          break;
        }
        receiveTotal += receive;
        receiveDataLeft -= receive;
      }
      return data;
    }

    public static async Task<byte[]> ReceiveAsyncFromTcp(this Socket client)
    {
      byte[] prefix = new byte[4];
      int receivedLength = await Task.Factory.FromAsync(
        (callback, state) => client.BeginReceive(prefix, 0, prefix.Length, SocketFlags.None, callback, state),
        client.EndReceive,
        null);

      if (receivedLength != prefix.Length)
        return null;

      int receiveSize = BitConverter.ToInt32(prefix, 0);
      byte[] result = new byte[receiveSize];
      int receiveDataLeft = receiveSize;
      int totalReceiveSize = 0;
      while (totalReceiveSize < receiveSize)
      {

        receivedLength = await Task.Factory.FromAsync(
          (callback, state) => client.BeginReceive(result, totalReceiveSize, receiveDataLeft, SocketFlags.None, callback, state),
          client.EndReceive,
          null);

        if (receivedLength == 0)
        {
          break;
        }

        totalReceiveSize += receiveSize;
        receiveDataLeft -= receiveSize;
      }

      // If content length is not appropriate return null to avoid deserialization bad structure.
      return receiveSize != result.Length ? null : result;
    }

    public static async Task<bool> SendAsync(this Socket client, byte[] data)
    {
      int sendingSize = data.Length;
      int sendingDataLeft = sendingSize;

      byte[] prefix = BitConverter.GetBytes(data.Length);
      //var prefixResetEvent = ManualResetEventPool.GetEvent();
      var result = await Task.Factory.FromAsync(
        (callback, state) => client.BeginSend(prefix, 0, prefix.Length, SocketFlags.None, callback, state),
        client.EndSend,
        null);

      if (result != prefix.Length)
        return false;

      result = await Task.Factory.FromAsync(
        (callback, state) => client.BeginSend(data, 0, data.Length, SocketFlags.None, callback, state),
        client.EndSend,
        null);

      return result == data.Length;
    }

    //public static void SendAsync(this Socket client, byte[] data, int timeoutInMilliseconds = 500)
    //{
    //  int sendingSize = data.Length;
    //  int sendingDataLeft = sendingSize;

    //  byte[] prefix = BitConverter.GetBytes(data.Length);
    //  var prefixResetEvent = ManualResetEventPool.GetEvent();

    //  client.BeginSend(prefix, 0, prefix.Length, SocketFlags.None, ar =>
    //  {
    //    if (client.EndSend(ar) != prefix.Length)
    //      throw new SocketException();

    //    prefixResetEvent.Accomplish();
    //  }, client);
    //  prefixResetEvent.WaitOne(timeoutInMilliseconds);

    //  var dataResetEvent = ManualResetEventPool.GetEvent();

    //  client.BeginSend(data, 0, data.Length, SocketFlags.None, ar =>
    //  {
    //    dataResetEvent.Accomplish();
    //  }, client);
    //  dataResetEvent.WaitOne(timeoutInMilliseconds);
    //}

    public static void SendTcp(this Socket client, ref byte[] data)
    {
      int sendingSize = data.Length;
      int sendingDataLeft = sendingSize;
      var sendPrefix = BitConverter.GetBytes(sendingSize);
      int sendingSent = client.Send(sendPrefix);
      int sendingTotal = 0;

      while (sendingTotal < sendingSize)
      {
        sendingSent = client.Send(data, sendingTotal, sendingDataLeft, SocketFlags.None);
        sendingTotal += sendingSent;
        sendingDataLeft -= sendingSent;
      }
    }

    private static byte[] _endOfLine = Convert.FromBase64String("\n");
    public static void SendTcpPipeline(this Socket client, ref byte[] data)
    {
      int sendingSize = data.Length;
      int sendingDataLeft = sendingSize;
      //var sendPrefix = BitConverter.GetBytes(sendingSize);
      int sendingSent = 0;//client.Send(sendPrefix);
      int sendingTotal = 0;

      while (sendingTotal < sendingSize)
      {
        sendingSent = client.Send(data, sendingTotal, sendingDataLeft, SocketFlags.None);
        sendingTotal += sendingSent;
        sendingDataLeft -= sendingSent;
      }
      client.Send(_endOfLine, 0, _endOfLine.Length, SocketFlags.None);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool ReceiveDataFromUdp(this UdpClient client, ref IPEndPoint ipEndPoint, out byte[] result)
    {
      return (result = (client?.Available > 0) ? client.Receive(ref ipEndPoint) : null)?.Length > 0;
    }

    public static async Task<byte[]> ReceiveDataFromUdpAsync(this UdpClient client)
    {
      if (!(client?.Available > 0)) return null;
      var result = await client.ReceiveAsync().ConfigureAwait(false);
      return result.Buffer;

    }

    public static async Task<bool> SendAsyncUdp(this UdpClient client, byte[] data)
    {
      if (client == null) return false;
      var sendLength = await client.SendAsync(data, data.Length).ConfigureAwait(false);
      return sendLength == data.Length;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void SendUdp(this UdpClient client, ref IPEndPoint ipEndPoint, ref byte[] data)
    {
      client?.Send(data, data.Length, ipEndPoint);
    }
  }
}
