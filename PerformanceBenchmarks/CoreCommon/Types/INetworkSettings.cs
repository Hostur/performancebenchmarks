﻿namespace CoreCommon
{
  public interface INetworkSettings
  {
    int ClientsCapacity { get; }
    bool ForwardPorts { get; }
    int DefaultTCPPort { get; }
    bool Async { get; }
  }
}
