﻿using System;
using System.Collections.Generic;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;

namespace CoreCommon
{
  public interface IRequestHandler
  {
    byte RequestId { get; }
    void Handle(int internalId, ref NetworkRequest request);
  }

  public abstract class RequestHandler : IRequestHandler
  {
    public static int HANDLED_REQUESTS;
    public static int SENT_REQUESTS;
    public abstract byte RequestId { get; }
    public abstract void Handle(int internalId, ref NetworkRequest request);
  }

  [CoreRegister(true)]
  public sealed class RequestHandlersDictionary
  {
    private readonly IRequestHandler[] _requestHandlersDictionary = new IRequestHandler[byte.MaxValue];

    public RequestHandlersDictionary(IList<IRequestHandler> handlers)
    {
      foreach (var handler in handlers)
        _requestHandlersDictionary[handler.RequestId] = handler;
    }

    public IRequestHandler this[byte requestId] => _requestHandlersDictionary[requestId];
  }

  [CoreRegister(true)]
  public sealed class GenericRequestHandler
  {
    private readonly RequestHandlersDictionary _requestHandlersDictionary;
    private readonly ICoreLogger _coreLogger;

    public GenericRequestHandler(RequestHandlersDictionary requestHandlersDictionary, ICoreLogger coreLogger)
    {
      _requestHandlersDictionary = requestHandlersDictionary;
      _coreLogger = coreLogger;
    }

    public void Handle(int internalId, ref NetworkRequest request)
    {
      try
      {
        _requestHandlersDictionary[request.Id]?.Handle(internalId, ref request);
      }
      catch (Exception e)
      {
        _coreLogger.Log($"Exception occur handling request id({request.Id.ToString()})\n{e}", LogType.Error);
      }
    }
  }
}
