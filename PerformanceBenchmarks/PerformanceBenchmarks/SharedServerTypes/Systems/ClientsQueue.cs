﻿using System;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils;
using CoreCommon.SharedUtils.Serialization;
using PerformanceBenchmarks.Server.Pipeline;

namespace PerformanceBenchmarks.Systems
{
  [CoreRegister(true)]
  public sealed class ClientsQueue : IDisposable
  {
    #region Injected
    private readonly ClientAssigned _clientAssigned;
    private readonly TCPClients _tcpClients;
    private readonly UDPClients _udpClients;
    private readonly UDPEndPoints _udpEndPoints;
    private readonly UdpPortsPool _udpPortsPool;
    private readonly ICoreLogger _coreLogger;

    // Pipeline
    private readonly NetworkStreams _networkStreams;
    private readonly PipelineReaders _pipelineReaders;
    private readonly PipeWriters _pipeWriters;
    #endregion

    private readonly Thread _readingUnconfirmedClientsThread;
    private readonly TcpListener _tcpListener;
    private readonly List<TcpClient> _tcpClientsInQueue;

    private readonly OutgoingTCPRequests _outgoingTcpRequests;
    private readonly OutgoingUDPRequests _outgoingUdpRequests;
    
    public ClientsQueue(
      ClientAssigned clientAssigned,
      TCPClients tcpClients,
      UDPClients udpClients,
      UDPEndPoints udpEndPoints, 
      UdpPortsPool udpPortsPool,
      INetworkSettings networkSettings,
      ICoreLogger coreLogger, NetworkStreams networkStreams, PipelineReaders pipelineReaders, PipeWriters pipeWriters,
      OutgoingTCPRequests outgoingTcpRequests,
      OutgoingUDPRequests outgoingUdpRequests)
    {
      _clientAssigned = clientAssigned;
      _tcpClients = tcpClients;
      _udpClients = udpClients;
      _udpEndPoints = udpEndPoints;
      _udpPortsPool = udpPortsPool;
      _coreLogger = coreLogger;
      _networkStreams = networkStreams;
      _pipelineReaders = pipelineReaders;
      _pipeWriters = pipeWriters;
      _outgoingTcpRequests = outgoingTcpRequests;
      _outgoingUdpRequests = outgoingUdpRequests;
      _tcpListener = new TcpListener(IPAddress.Any, networkSettings.DefaultTCPPort);
      _tcpClientsInQueue = new List<TcpClient>(networkSettings.ClientsCapacity / 10);
      _readingUnconfirmedClientsThread = new Thread(ReadingFromUnconfirmedSocketsUpdate);
      _readingUnconfirmedClientsThread.IsBackground = false;
    }

    public void Start()
    {
      try
      {
        _tcpListener.Start();
        _readingUnconfirmedClientsThread.Start();
        StartListening();
        _coreLogger.Log("Clients queue started.", LogType.Info);
      }
      catch (Exception e)
      {
        _coreLogger.Log($"Exception occur during starting clients queue: {e}", LogType.Error);
      }
    }

    private void StartListening() => _tcpListener?.BeginAcceptTcpClient(AcceptTcpClient, _tcpListener);

    private void AcceptTcpClient(IAsyncResult asyncResult)
    {
      try
      {
        _coreLogger.Log("New incoming tcp client.", LogType.Info);
        TcpListener listener = (TcpListener)asyncResult.AsyncState;
        TcpClient tmpTcpClient = listener.EndAcceptTcpClient(asyncResult);

        OnClientConnected(tmpTcpClient);
      }
      catch (Exception e)
      {
        _coreLogger.Log($"Exception occur during AcceptTcpClient. {e.Message}", LogType.Error);
      }
      finally
      {
        StartListening();
      }
    }

    private void OnClientConnected(TcpClient client)
    {
      try
      {
        _coreLogger.Log("Sending ConfirmConnectionRequest to new serverNetworkClient.", LogType.Debug);

        _tcpClientsInQueue.Add(client);

        var networkRequest = new NetworkRequest(ConfirmationRequest.REQUEST_IDENTIFIER, new ConfirmationRequest(ConfirmationState.RequestConfirmation).Serialize());
        var output = new NetworkRequest[] { networkRequest }.SerializeArray();
        client.Client.SendTcp(ref output);
      }
      catch (Exception e)
      {
        Console.WriteLine("Exception occur during OnClientConnection: " + e);
      }
    }

    public void ReadingFromUnconfirmedSocketsUpdate()
    {
      try
      {
        while (true)
        {
          for (int i = 0; i < _tcpClientsInQueue.Count; i++)
          {
            if (ListenNonConfirmedClients(_tcpClientsInQueue[i]))
            {
              _tcpClientsInQueue.RemoveAt(i);
              if (i > 0)
              {
                i--;
              }
            }
          }
          Thread.Sleep(300);
        }
      }
      catch (Exception e)
      {
        _coreLogger.Log($"Exception occur during ReadingFromUnconfirmedSocketsUpdate: {e}", LogType.Error);
      }
    }

    private bool ListenNonConfirmedClients(TcpClient tcpClient)
    {
      try
      {
        if (tcpClient.GetStream().DataAvailable)
        {
          var data = tcpClient.Client.ReceiveFromTcp();
          if (data.Length != 0)
          {
            _coreLogger.Log($"Receive request from unconfirmed serverNetworkClient.", LogType.Debug);
            var requests = data.DeserializeArray<NetworkRequest>();

            for (int i = 0; i < requests.Length; i++)
            {
              if (requests[i].Id == ConfirmationRequest.REQUEST_IDENTIFIER)
              {
                return OnClientConfirm(tcpClient, requests[i].Data.Deserialize<ConfirmationRequest>());
              }

              _coreLogger.Log("Bad confirm request from serverNetworkClient. Probably serverNetworkClient sent something before ClientConfirmNewConnectionRequest request.", LogType.Warning);
            }
          }
        }

        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.ToString());
        return false;
      }
    }

    private bool OnClientConfirm(TcpClient tcpClient, ConfirmationRequest confirm)
    {
      _coreLogger.Log($"Confirmation request: State = {confirm.ConfirmationState}, Authorization token: {confirm.AuthorizationToken}.", LogType.Debug);
      if (confirm.ConfirmationState != ConfirmationState.Confirmation || confirm.AuthorizationToken == null)
      {
        _coreLogger.Log("Confirmation state is not Confirmation or authorization token is null.", LogType.Error);
        return false;
      }

      if (_udpPortsPool.GetAvailableUdpPortNumber(out int udpPort))
      {
        int internalId = _clientAssigned.Add(_tcpClients, tcpClient);

        _outgoingUdpRequests.TryDequeue(internalId, out _);
        _outgoingTcpRequests.TryDequeue(internalId, out _);

        // Pipeline
        _networkStreams[internalId] = new NetworkStream(tcpClient.Client);
        _pipeWriters[internalId] = PipeWriter.Create(_networkStreams[internalId]);
        _pipelineReaders[internalId] = PipeReader.Create(_networkStreams[internalId]);


        // FOR DEVELOPMENT, mapp udp port.
        _udpEndPoints.SetEndpoint(internalId, udpPort);
        UdpClient client = new UdpClient(_udpEndPoints[internalId]);
        client.Client.ReceiveBufferSize = 64000;
        client.Client.SendBufferSize = 64000;
        client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 200);
        _udpClients[internalId] = client;

        _coreLogger.Log($"Successful added serverNetworkClient. Port UDP: {udpPort.ToString()}", LogType.Debug);
        SendConfirmation(tcpClient, udpPort, internalId);

        return true;
      }

      _coreLogger.Log($"Cannot confirm new client because there is no more available UDP ports in pool.", LogType.Debug);
      return false;
    }

    private void SendConfirmation(TcpClient client, int udpPort, int networkId)
    {
      var request = new NetworkRequest(ConfirmationRequest.REQUEST_IDENTIFIER,
        new ConfirmationRequest(udpPort, networkId).Serialize());
     var response = new NetworkRequest[] { request }.SerializeArray();
      client.Client.SendTcp(ref response);
    }

    public void Dispose()
    {
      _tcpListener?.Stop();
      _readingUnconfirmedClientsThread?.Abort();
    }
  }
}
