﻿using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks.SharedServerTypes
{
  [CoreRegister(true)]
  public class NetworkSettings : INetworkSettings
  {
    public int ClientsCapacity { get; set; }

    public bool ForwardPorts { get; set; }

    public int DefaultTCPPort => 6331;

    public bool Async => false;
  }
}
