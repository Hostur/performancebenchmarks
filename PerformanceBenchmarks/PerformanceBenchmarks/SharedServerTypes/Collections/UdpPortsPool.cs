﻿using System;
using System.Collections.Generic;
using CoreCommon;
using CoreCommon.DI.Attributes;
namespace PerformanceBenchmarks
{
  [CoreRegister(true)]
  public class UdpPortsPool
  {
    private readonly Stack<int> _ports;
    public UdpPortsPool(INetworkSettings settings)
    {
      _ports = new Stack<int>(settings.ClientsCapacity);
      int start = settings.DefaultTCPPort;
      int ends = start + settings.ClientsCapacity + 1;
      for (int i = start; i < ends; i++)
      {
        _ports.Push(i);
      }
    }

    public bool GetAvailableUdpPortNumber(out int result)
    {
      lock (_ports)
      {
        if (_ports.Count > 0)
        {
          result = _ports.Pop();
          return true;
        }

        result = 0;
        return false;
      }
    }

    public void PushBackFreePort(int? port)
    {
      if (!port.HasValue) return;
      lock (_ports)
      {
        if (_ports.Contains(port.Value)) return;
        Console.WriteLine($"Pushing back port {port.Value} to pool.");
        _ports.Push(port.Value);
      }
    }
  }
}