﻿using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks
{
  [CoreRegister(true)]
  public sealed class UdpReadiness
  {
    private readonly bool[] _ready;

    public UdpReadiness(INetworkSettings settings)
    {
      _ready = new bool[settings.ClientsCapacity];
    }

    public bool this[int internalId]
    {
      get => _ready[internalId];
      set => _ready[internalId] = value;
    }
  }
}