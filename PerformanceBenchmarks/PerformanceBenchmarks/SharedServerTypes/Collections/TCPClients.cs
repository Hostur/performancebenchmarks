﻿using System.Net.Sockets;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks
{
  [CoreRegister(true)]
  public sealed class TCPClients
  {
    private readonly TcpClient[] _clients;

    public TCPClients(INetworkSettings settings)
    {
      _clients = new TcpClient[settings.ClientsCapacity];
    }

    public TcpClient this[int internalId]
    {
      get => _clients[internalId];
      set => _clients[internalId] = value;
    }
  }
}
