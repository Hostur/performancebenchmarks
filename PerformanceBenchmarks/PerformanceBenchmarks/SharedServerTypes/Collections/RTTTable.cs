﻿using System.Linq;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks
{
  [CoreRegister(true)]
  public sealed class RTTTable
  {
    private readonly CollectionsExtension.FixedBuffer<int>[] _values;

    public RTTTable(INetworkSettings settings)
    {
      _values = new CollectionsExtension.FixedBuffer<int>[settings.ClientsCapacity];
      for (int i = 0; i < settings.ClientsCapacity; i++)
      {
        _values[i] = new CollectionsExtension.FixedBuffer<int>(10);
      }
    }

    public void AddValue(int internalId, int value) => _values[internalId].Push(value);
    public int RTT(int internalId) => (int) _values[internalId].GetAllElements.Average();

    /// <summary>
    /// Get average RTT for all the connected clients.
    /// </summary>
    public int GlobalRTT(int clientsAssigned)
    {
      int[] values = new int[clientsAssigned];
      for (int i = 0; i < clientsAssigned; i++)
        values[i] = RTT(i);

      return (int) values.Average();
    }
  }
}
