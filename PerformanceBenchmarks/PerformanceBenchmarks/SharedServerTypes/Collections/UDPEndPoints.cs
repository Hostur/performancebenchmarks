﻿using System.Net;
using CoreCommon;
using CoreCommon.DI.Attributes;


namespace PerformanceBenchmarks
{
  [CoreRegister(true)]
  public sealed class UDPEndPoints
  {
    private readonly IPEndPoint[] _ipEndPoints;
    private readonly MappedUDPPorts _mappedUdpPorts;
    public UDPEndPoints(INetworkSettings settings, MappedUDPPorts mappedUdpPorts)
    {
      _mappedUdpPorts = mappedUdpPorts;
      _ipEndPoints = new IPEndPoint[settings.ClientsCapacity];
    }

    public ref IPEndPoint this[int internalId] => ref _ipEndPoints[internalId];

    public void SetEndpoint(int internalId, int port)
    {
      _ipEndPoints[internalId] = new IPEndPoint(IPAddress.Any, _mappedUdpPorts.GetMappedPort(port));
    }
  }
}
