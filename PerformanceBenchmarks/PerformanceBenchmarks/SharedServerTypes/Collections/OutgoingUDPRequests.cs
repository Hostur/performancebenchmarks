﻿using System;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;

namespace PerformanceBenchmarks
{
  [CoreRegister(true)]
  public sealed class OutgoingUDPRequests
  {
    private readonly NetworkRequestsQueue[] _requestsQueue;

    public OutgoingUDPRequests(INetworkSettings networkSettings)
    {
      _requestsQueue = new NetworkRequestsQueue[networkSettings.ClientsCapacity];
      for (int i = 0; i < networkSettings.ClientsCapacity; i++)
        _requestsQueue[i] = new NetworkRequestsQueue(100);
    }

    public void Enqueue(int internalId, ref NetworkRequest request) => _requestsQueue[internalId].Enqueue(ref request);
    public bool TryDequeue(int internalId, out NetworkRequest[] requests) => _requestsQueue[internalId].TryDequeue(out requests);
  }
}
