﻿using System.Collections.Generic;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks
{
  /// <summary>
  /// If we are testing server on machine that need to forwards ports we store mapped udp ports here to use them while our clients connecting.
  /// </summary>
  [CoreRegister(true)]
  public sealed class MappedUDPPorts
  {
    private readonly Dictionary<int, int> _mappedUdpPorts;

    public MappedUDPPorts(INetworkSettings settings)
    {
      _mappedUdpPorts = new Dictionary<int, int>(settings.ClientsCapacity);
    }

    public void AddMappedPort(int mapped, int port)
    {
      _mappedUdpPorts.AddIfNotExists(mapped, port);
    }

    public int GetMappedPort(int port)
    {
      if (_mappedUdpPorts.TryGetValue(port, out int value))
        return value;
      return port;
    }
  }
}
