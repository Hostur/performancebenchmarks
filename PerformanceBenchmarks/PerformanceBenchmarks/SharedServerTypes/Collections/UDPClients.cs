﻿using System.Net.Sockets;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks
{
  [CoreRegister(true)]
  public sealed class UDPClients
  {
    private readonly UdpClient[] _clients;

    public UDPClients(INetworkSettings settings)
    {
      _clients = new UdpClient[settings.ClientsCapacity];
    }

    public UdpClient this[int internalId]
    {
      get => _clients[internalId];
      set => _clients[internalId] = value;
    }
  }
}
