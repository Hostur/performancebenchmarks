﻿using System;
using System.Net.Sockets;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks
{
  [CoreRegister(true)]
  public sealed class ClientAssigned
  {
    private readonly bool[] _ready;
    private readonly int _capacity;
    public ClientAssigned(INetworkSettings settings)
    {
      _capacity = settings.ClientsCapacity;
      _ready = new bool[settings.ClientsCapacity];
    }

    public int Add()
    {
      lock (_ready)
      {
        for (int i = 0; i < _capacity; i++)
        {
          if (!this[i])
          {
            this[i] = true;
            return i;
          }
        }
      }
      throw new Exception("No available ");
    }

    public int Add(TCPClients tcpClients, TcpClient client)
    {
      lock (_ready)
      {
        for (int i = 0; i < _capacity; i++)
        {
          if (!this[i])
          {
            tcpClients[i] = client;
            this[i] = true;
            return i;
          }
        }
      }
      throw new Exception("No available ");
    }

    public int Count()
    {
      lock (_ready)
      {
        for (int i = 0; i < _capacity; i++)
        {
          if (!this[i])
          {
            return i;
          }
        }
      }
      throw new Exception("No available ");
    }

    public bool this[int internalId]
    {
      get
      {
        lock(_ready)
          return _ready[internalId];
      }
      set
      {
        lock(_ready)
          _ready[internalId] = value;
      }
    }
  }
}
