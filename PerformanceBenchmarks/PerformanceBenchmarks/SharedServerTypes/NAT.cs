﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Open.Nat;

// ReSharper disable InconsistentNaming

namespace PerformanceBenchmarks
{
  public static class NAT
  {
    public static async Task<bool> RemoveMappingOnThisIp()
    {
      try
      {
        var nat = new NatDiscoverer();
        var cts = new CancellationTokenSource();
        cts.CancelAfter(5000);
        NatDevice device = await nat.DiscoverDeviceAsync(PortMapper.Upnp, cts).ConfigureAwait(false);
        IPAddress privateIP = await GetLocalIPAddress().ConfigureAwait(false);
        var mapping = await device.GetAllMappingsAsync().ConfigureAwait(false);

        foreach (Mapping m in mapping.Where(m => Equals(m.PrivateIP, privateIP)))
        {
          await device.DeletePortMapAsync(new Mapping(m.Protocol, m.PrivatePort, m.PublicPort));
        }
        return true;
      }
      catch
      {
        return false;
      }
    }

    public static async Task<int> CreateMapping(int publicPort, Protocol protocol, string mappingName = "", int cancelAfter = 5000)
    {
      if (string.IsNullOrEmpty(mappingName))
      {
        mappingName = string.Format("Default mapping name ({0})", protocol.ToString());
      }
      var nat = new NatDiscoverer();
      var cts = new CancellationTokenSource();
      cts.CancelAfter(cancelAfter);

      NatDevice device = await nat.DiscoverDeviceAsync(PortMapper.Upnp, cts).ConfigureAwait(false);
      IPAddress privateIP = await GetLocalIPAddress().ConfigureAwait(false);
      var mapping = await device.GetAllMappingsAsync().ConfigureAwait(false);

      foreach (Mapping m in mapping.Where(m => m.Protocol == protocol && Equals(m.PrivateIP, privateIP) && m.PrivatePort == publicPort))
      {
        await device.DeletePortMapAsync(new Mapping(m.Protocol, m.PrivatePort, m.PublicPort));
      }

      int port = 0;
      for (int i = publicPort; i < 65534; i++)
      {
        if (mapping.Any(m => m.PublicPort == i))
        {
          continue;
        }
        port = i;
        break;
      }

      if (port == 0)
      {
        throw new Exception("Mapping failed: The available port was not found.");
      }
      await device.CreatePortMapAsync(new Mapping(protocol, publicPort, port, 0, mappingName));
      return port;
    }

    private static async Task<IPAddress> GetLocalIPAddress()
    {
      var host = await Dns.GetHostEntryAsync(Dns.GetHostName()).ConfigureAwait(false);
      foreach (var ip in host.AddressList)
      {
        if (ip.AddressFamily == AddressFamily.InterNetwork)
        {
          return ip;
        }
      }
      return IPAddress.None;
    }
  }
}
