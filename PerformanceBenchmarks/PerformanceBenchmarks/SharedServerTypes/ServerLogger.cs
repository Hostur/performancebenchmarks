﻿using System;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks.SharedServerTypes
{
  [CoreRegister(true)]
  public sealed class ServerLogger : ICoreLogger
  {
    private ConsoleColor GetColor(LogType logType)
    {
      switch (logType)
      {
        case LogType.Debug:
          return ConsoleColor.White;
        case LogType.Info:
          return ConsoleColor.Blue;
        case LogType.Warning:
          return ConsoleColor.Yellow;
        default:
          return ConsoleColor.Red;
      }
    }
    public void Log(string log, LogType logType)
    {
      Console.ForegroundColor = GetColor(logType);
      Console.WriteLine(log);
    }
  }
}
