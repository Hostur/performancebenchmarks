﻿using System;
using System.Buffers;
using System.Net.Sockets;
using System.Threading.Tasks;

//using System.Runtime.CompilerServices;

namespace PerformanceBenchmarks.Logic
{
  public static partial class TcpUdpExtension
  {
    //[MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ReceiveFromTcp(this TcpClient client, ref byte[] receivePrefix, in ArrayPool<byte> pool, out byte[] data)
    {
      int receiveTotal = 0;

      int receive = client.Client.Receive(receivePrefix, 0, 4, 0);
      int receiveSize = BitConverter.ToInt32(receivePrefix, 0);
      Array.Clear(receivePrefix, 0, receivePrefix.Length);
      int receiveDataLeft = receiveSize;

      data = pool.Rent(receiveSize);
      Array.Clear(data, 0, data.Length);
      while (receiveTotal < receiveSize)
      {
        receive = client.Client.Receive(data, receiveTotal, receiveDataLeft, 0);
        if (receive == 0)
        {
          break;
        }
        receiveTotal += receive;
        receiveDataLeft -= receive;
      }
    }

    //public static async ValueTask SendTcpAsync(this Socket client, ReadOnlyMemory<byte> data)
    //{
    //  int sendingSize = data.Length;
    //  int sendingDataLeft = sendingSize;
    //  var sendPrefix = BitConverter.GetBytes(sendingSize);
    //  int sendingSent = await client.SendAsync(new ReadOnlyMemory<byte>(sendPrefix), SocketFlags.None).ConfigureAwait(false);
    //  int sendingTotal = 0;

    //  while (sendingTotal < sendingSize)
    //  {
    //    sendingSent = await client.SendAsync(data, sendingTotal, sendingDataLeft, SocketFlags.None).ConfigureAwait(false);
    //    sendingTotal += sendingSent;
    //    sendingDataLeft -= sendingSent;
    //  }
    //}
  }
}
