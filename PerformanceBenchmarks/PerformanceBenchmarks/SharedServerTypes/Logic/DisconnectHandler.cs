﻿using System;
using System.Net.Sockets;
using CoreCommon;
using CoreCommon.DI.Attributes;
using PerformanceBenchmarks.Server.Pipeline;

namespace PerformanceBenchmarks.Logic
{
  [CoreRegister(true)]
  public sealed class DisconnectHandler
  {
    private readonly ClientAssigned _clientAssigned;
    private readonly UdpPortsPool _udpPortsPool;
    private readonly UDPClients _udpClients;
    private readonly UDPEndPoints _udpEndPoints;
    private readonly TCPClients _tcpClients;
    private readonly OutgoingUDPRequests _outgoingUdpRequests;
    private readonly OutgoingTCPRequests _outgoingTcpRequests;
    private readonly ICoreLogger _coreLogger;

    private readonly NetworkStreams _networkStreams;

    public DisconnectHandler(
      ClientAssigned clientAssigned,
      UdpPortsPool udpPortsPool,
      UDPClients udpClients,
      TCPClients tcpClients,
      OutgoingUDPRequests outgoingUdpRequests,
      OutgoingTCPRequests outgoingTcpRequests,
      UDPEndPoints udpEndPoints, ICoreLogger coreLogger, NetworkStreams networkStreams)
    {
      _clientAssigned = clientAssigned;
      _udpPortsPool = udpPortsPool;
      _udpClients = udpClients;
      _tcpClients = tcpClients;
      _outgoingUdpRequests = outgoingUdpRequests;
      _outgoingTcpRequests = outgoingTcpRequests;
      _udpEndPoints = udpEndPoints;
      _coreLogger = coreLogger;
      _networkStreams = networkStreams;
    }

    public void Disconnect(int internalId)
    {
      try
      {
        if (!_clientAssigned[internalId])
          return;
        _coreLogger.Log($"Disconnecting client: {internalId}", LogType.Info);
        _clientAssigned[internalId] = false;

        _networkStreams[internalId]?.Flush();
        _networkStreams[internalId]?.Dispose();
        _networkStreams[internalId] = null;

        _udpPortsPool.PushBackFreePort(_udpEndPoints[internalId].Port);
        _udpEndPoints[internalId] = null;
        _udpClients[internalId]?.Dispose();
        _udpClients[internalId] = null;
        _tcpClients[internalId]?.Dispose();
        _tcpClients[internalId] = null;
        _outgoingUdpRequests.TryDequeue(internalId, out _);
        _outgoingTcpRequests.TryDequeue(internalId, out _);
      }
      catch (Exception e)
      {
        _coreLogger.Log($"Exception occur during disconnecting client: {internalId}\n {e}", LogType.Error);
      }
    }
  }
}
