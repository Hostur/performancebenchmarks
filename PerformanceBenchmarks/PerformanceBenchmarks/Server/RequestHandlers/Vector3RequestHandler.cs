﻿using System;
using System.Buffers;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils.Serialization;

namespace PerformanceBenchmarks.Server.RequestHandlers
{
  [CoreRegister(true)]
  public sealed class Vector3RequestHandler : RequestHandler
  {
    public override byte RequestId => Vector3Request.REQUEST_IDENTIFIER;
    private readonly ArrayPool<byte> _arrayPool = ArrayPool<byte>.Shared;
    private readonly OutgoingTCPRequests _outgoingTcpRequests;
    private readonly OutgoingUDPRequests _outgoingUdpRequests;

    public Vector3RequestHandler(OutgoingTCPRequests outgoingTcpRequests, OutgoingUDPRequests outgoingUdpRequests)
    {
      _outgoingTcpRequests = outgoingTcpRequests;
      _outgoingUdpRequests = outgoingUdpRequests;
    }

    public override void Handle(int internalId, ref NetworkRequest request)
    {
      Vector3Request req = request.Data.Deserialize<Vector3Request>();
      ++HANDLED_REQUESTS;
      SENT_REQUESTS += 2;
      _outgoingUdpRequests.Enqueue(internalId, ref request);
      _outgoingTcpRequests.Enqueue(internalId, ref request);
    }
  }
}
