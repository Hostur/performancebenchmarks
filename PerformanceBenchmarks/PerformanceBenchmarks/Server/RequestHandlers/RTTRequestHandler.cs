﻿using System;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils.Serialization;

namespace PerformanceBenchmarks.Server.RequestHandlers
{
  [CoreRegister(true)]
  internal sealed class RTTRequestHandler : RequestHandler
  {
    private readonly RTTTable _rttTable;

    public RTTRequestHandler(RTTTable rttTable)
    {
      _rttTable = rttTable;
    }

    public override byte RequestId => RTTRequest.REQUEST_IDENTIFIER;
    public override void Handle(int internalId, ref NetworkRequest request)
    {
      ++HANDLED_REQUESTS;
      RTTRequest req = ZeroSerializer.Deserialize<RTTRequest>(request.Data);
      _rttTable.AddValue(internalId, (DateTime.Now - req.ServerSendTime).Milliseconds);
    }
  }
}
