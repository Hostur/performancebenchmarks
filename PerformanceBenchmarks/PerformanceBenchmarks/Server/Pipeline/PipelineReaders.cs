﻿using System.IO.Pipelines;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks.Server.Pipeline
{
  [CoreRegister(true)]
  public sealed class PipelineReaders
  {
    private readonly PipeReader[] _readers;

    public PipelineReaders(INetworkSettings settings)
    {
      _readers = new PipeReader[settings.ClientsCapacity];
    }

    public PipeReader this[int internalId]
    {
      get => _readers[internalId];
      set => _readers[internalId] = value;
    }
  }
}