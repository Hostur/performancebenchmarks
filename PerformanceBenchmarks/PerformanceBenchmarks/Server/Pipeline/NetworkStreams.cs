﻿using System;
using System.Net.Sockets;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks.Server.Pipeline
{
  [CoreRegister(true)]
  public sealed class NetworkStreams : IDisposable
  {
    private readonly NetworkStream[] _streams;

    public NetworkStreams(INetworkSettings settings)
    {
      _streams = new NetworkStream[settings.ClientsCapacity];
    }

    public NetworkStream this[int internalId]
    {
      get => _streams[internalId];
      set => _streams[internalId] = value;
    }

    public async void Dispose()
    {
      foreach (NetworkStream networkStream in _streams)
      {
        if(networkStream != null)
          await networkStream.DisposeAsync().ConfigureAwait(false);
      }
    }
  }
}
