﻿using System;
using CoreCommon;
using CoreCommon.DI.Attributes;
using PerformanceBenchmarks.Server.Legacy;
using PerformanceBenchmarks.Systems;

namespace PerformanceBenchmarks.Server.Pipeline
{
  [CoreRegister(true)]
  public sealed class PipelineServer : IDisposable
  {
    private readonly ClientsQueue _clientsQueue;
    private readonly TcpPipelineSystem _tcpPipelineSystem;
    private readonly UdpSystem _udpSystem;
    private readonly INetworkSettings _networkSettings;

    public PipelineServer(TcpPipelineSystem tcpPipelineSystem, ClientsQueue clientsQueue, INetworkSettings networkSettings, UdpSystem udpSystem)
    {
      _tcpPipelineSystem = tcpPipelineSystem;
      _clientsQueue = clientsQueue;
      _networkSettings = networkSettings;
      _udpSystem = udpSystem;
    }

    public void Start()
    {
      _clientsQueue.Start();
      _tcpPipelineSystem.Start(_networkSettings.ClientsCapacity);
      _udpSystem.Start(_networkSettings.ClientsCapacity);
    }

    ~PipelineServer()
    {
      Dispose();
    }

    public void Dispose()
    {
    }
  }
}
