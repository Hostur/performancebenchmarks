﻿using System;
using System.Buffers;
using System.IO.Pipelines;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils;
using CoreCommon.SharedUtils.Serialization;
using PerformanceBenchmarks.Logic;

namespace PerformanceBenchmarks.Server.Pipeline
{
  [CoreRegister(true)]
  public class TcpPipelineSystem : IDisposable
  {
    private readonly ClientAssigned _clientAssigned;
    private readonly OutgoingTCPRequests _outgoingTcpRequests;
    private readonly DisconnectHandler _disconnectionHandler;
    private readonly GenericRequestHandler _genericRequestHandler;
    private readonly TCPClients _tcpClients;
    private readonly ICoreLogger _coreLogger;

    private readonly NetworkStreams _networkStreams;
    private readonly PipelineReaders _pipelineReaders;
    private readonly PipeWriters _pipeWriters;
 
    private Thread _tcpThread;

    public TcpPipelineSystem(
      OutgoingTCPRequests outgoingTcpRequests,
      DisconnectHandler disconnectionHandler,
      GenericRequestHandler genericRequestHandler,
      ClientAssigned clientAssigned,
      TCPClients tcpClients,
      ICoreLogger coreLogger, NetworkStreams networkStreams, PipelineReaders pipelineReaders, PipeWriters pipeWriters)
    {
      _outgoingTcpRequests = outgoingTcpRequests;
      _disconnectionHandler = disconnectionHandler;
      _genericRequestHandler = genericRequestHandler;
      _clientAssigned = clientAssigned;
      _tcpClients = tcpClients;
      _coreLogger = coreLogger;
      _networkStreams = networkStreams;
      _pipelineReaders = pipelineReaders;
      _pipeWriters = pipeWriters;
    }

    public void Start(int serverCapacity)
    {
      ProceedClients(serverCapacity);
      _coreLogger.Log("TCP system started.", LogType.Info);
    }

    private void ProceedClients(int serverCapacity)
    {
      _tcpThread = new Thread(async () => await ProceedClients(1, serverCapacity).ConfigureAwait(false))
      {
        IsBackground = true
      };
      _tcpThread.Start();
    }
    private async Task ProceedClients(int start, int end)
    {
      while (true)
      {
        for (int i = start; i < end; i++)
        {
          if (_clientAssigned[i])
          {
            await HandleTcp(i).ConfigureAwait(false);
          }
        }
      }
    }

    private async Task HandleTcp(int internalId)
    {
      try
      {
        var reader = _pipelineReaders[internalId];
        ReadResult tmpResult = await reader.ReadAsync().ConfigureAwait(false);
        ReadOnlySequence<byte> buffer = tmpResult.Buffer;

        // Can't because async :(
        // ReadOnlySpan<byte> prefix = Buffer.FirstSpan.Slice(0, 4);

        var start = buffer.Start;
        if (buffer.TryGet(ref start, out ReadOnlyMemory<byte> internalBuffer, true))
        {
          int incomingRequestLength = BitConverter.ToInt32(internalBuffer.Slice(0, 4).ToArray(), 0);

          var request = await reader.ReadAsync().ConfigureAwait(false);
          ReadOnlySequence<byte> requestBuffer = request.Buffer;

          while (requestBuffer.Length < incomingRequestLength)
          {
            requestBuffer = (await reader.ReadAsync().ConfigureAwait(false)).Buffer;
          }

          // Zero formatter deserialization.
          NetworkRequest[] requests = requestBuffer.ToArray().DeserializeArray<NetworkRequest>();

          // Return Buffer
          reader.AdvanceTo(buffer.Start, buffer.End);
          reader.AdvanceTo(requestBuffer.Start, requestBuffer.End);
          for (int i = 0; i < requests.Length; i++)
          {
            _genericRequestHandler.Handle(internalId, ref requests[i]);
          }
        }

        // Ugly to array method.
        //int incomingRequestLength = BitConverter.ToInt32(Buffer.Slice(Buffer.Start, 4).ToArray(), 0);

        













        //if (_outgoingTcpRequests.TryDequeue(internalId, out NetworkRequest[] output))
        //{
        //  var tmp = output.SerializeArray();
        //  tmp = new 
        //  ReadOnlyMemory<byte> bytes = new ReadOnlyMemory<byte>(output.SerializeArray());



        //  await _pipeWriters[internalId].WriteAsync(bytes).ConfigureAwait(false);
        //  await _pipeWriters[internalId].FlushAsync().ConfigureAwait(false);
        //}
      }
      catch (Exception e)
      {
        Console.WriteLine("Exception occur in TCP: " + e);
        _disconnectionHandler.Disconnect(internalId);
      }
    }

    private static bool TryReadLine(ref ReadOnlySequence<byte> buffer, out ReadOnlySequence<byte> line)
    {
      // Look for a EOL in the Buffer.
      SequencePosition? position = buffer.PositionOf((byte)'\n');

      if (position == null)
      {
        line = default;
        return false;
      }

      // Skip the line + the \n.
      line = buffer.Slice(0, position.Value);
      buffer = buffer.Slice(buffer.GetPosition(1, position.Value));
      return true;
    }

    public void Dispose()
    {
      _tcpThread?.Abort();
    }
  }
}