﻿using System.IO.Pipelines;
using CoreCommon;
using CoreCommon.DI.Attributes;

namespace PerformanceBenchmarks.Server.Pipeline
{
  [CoreRegister(true)]
  public sealed class PipeWriters
  {
    private readonly PipeWriter[] _readers;

    public PipeWriters(INetworkSettings settings)
    {
      _readers = new PipeWriter[settings.ClientsCapacity];
    }

    public PipeWriter this[int internalId]
    {
      get => _readers[internalId];
      set => _readers[internalId] = value;
    }
  }
}