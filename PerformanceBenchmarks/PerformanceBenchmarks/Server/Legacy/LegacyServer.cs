﻿using System;
using BenchmarkDotNet.Attributes;
using CoreCommon;
using CoreCommon.DI;
using CoreCommon.DI.Attributes;
using PerformanceBenchmarks.Systems;

namespace PerformanceBenchmarks.Server.Legacy
{
  [MemoryDiagnoser]
  [CoreRegister(true)]
  public class LegacyServer : IDisposable
  {
    private readonly ClientsQueue _clientsQueue;
    private readonly TcpSystem _tcpSystem;
    private readonly UdpSystem _udpSystem;
    private readonly ICoreLogger _coreLogger;
    private readonly INetworkSettings _networkSettings;

    public LegacyServer(
      ClientsQueue clientsQueue,
      TcpSystem tcpSystem, 
      UdpSystem udpSystem,
      ICoreLogger coreLogger,
      INetworkSettings networkSettings)
    {
      _clientsQueue = clientsQueue;
      _tcpSystem = tcpSystem;
      _udpSystem = udpSystem;
      _coreLogger = coreLogger;
      _networkSettings = networkSettings;
    }

    public LegacyServer()
    {
      _clientsQueue = God.PrayFor<ClientsQueue>();
      _tcpSystem = God.PrayFor<TcpSystem>();
      _udpSystem = God.PrayFor<UdpSystem>();
      _coreLogger = God.PrayFor<ICoreLogger>();
      _networkSettings = God.PrayFor<INetworkSettings>();
    }

    public void Start()
    {
      _coreLogger.Log("Server starting..", LogType.Info);
      _clientsQueue.Start();
      _tcpSystem.Start(_networkSettings.ClientsCapacity);
      _udpSystem.Start(_networkSettings.ClientsCapacity);
      _coreLogger.Log("Server running.", LogType.Info);
    }

    ~LegacyServer()
    {
      Dispose();
    }

    public void Dispose()
    {
      _clientsQueue?.Dispose();
      _tcpSystem?.Dispose();
      _udpSystem?.Dispose();
    }
  }
}
