﻿using System;
using System.Buffers;
using System.Net.Sockets;
using System.Threading;
using BenchmarkDotNet.Attributes;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils;
using CoreCommon.SharedUtils.Serialization;
using PerformanceBenchmarks.Logic;

namespace PerformanceBenchmarks.Server.Legacy
{
  [CoreRegister(true)]
  public class TcpSystem : IDisposable
  {
    private readonly ClientAssigned _clientAssigned;
    private readonly OutgoingTCPRequests _outgoingTcpRequests;
    private readonly DisconnectHandler _disconnectionHandler;
    private readonly GenericRequestHandler _genericRequestHandler;
    private readonly TCPClients _tcpClients;
    private readonly ICoreLogger _coreLogger;
    private readonly INetworkSettings _networkSettings;
    private Thread _tcpThread;

    public TcpSystem(
      OutgoingTCPRequests outgoingTcpRequests,
      DisconnectHandler disconnectionHandler,
      GenericRequestHandler genericRequestHandler,
      ClientAssigned clientAssigned,
      TCPClients tcpClients,
      ICoreLogger coreLogger,
      INetworkSettings networkSettings)
    {
      _outgoingTcpRequests = outgoingTcpRequests;
      _disconnectionHandler = disconnectionHandler;
      _genericRequestHandler = genericRequestHandler;
      _clientAssigned = clientAssigned;
      _tcpClients = tcpClients;
      _coreLogger = coreLogger;
      _networkSettings = networkSettings;
    }

    public void Start(int serverCapacity)
    {
      ProceedClients(serverCapacity);
      _coreLogger.Log("TCP system started.", LogType.Info);
    }
    private void ProceedClients(int serverCapacity)
    {
      _tcpThread = new Thread(() => ProceedClients(1, serverCapacity))
      {
        IsBackground = true
      };
      _tcpThread.Start();
    }
    private void ProceedClients(int start, int end)
    {
      ArrayPool<byte> pool = ArrayPool<byte>.Shared;
      byte[] receivePrefix = new byte[4];

      while (true)
      {
        for (int i = start; i < end; i++)
        {
          if (_clientAssigned[i])
          {
            HandleTcp(i, ref receivePrefix, in pool);
          }
        }
      }
    }

    private void HandleTcp(int internalId, ref byte[] receivePrefix, in ArrayPool<byte> pool)
    {
      try
      {
        TcpClient client = _tcpClients[internalId];
        if (!client.Connected)
        {
          _disconnectionHandler.Disconnect(internalId);
          return;
        }

        if (_outgoingTcpRequests.TryDequeue(internalId, out NetworkRequest[] result))
        {
          var bytes = result.SerializeArray();
          client.Client.SendTcp(ref bytes);
        }


        if (!_tcpClients[internalId].GetStream().DataAvailable)
          return;

        // result is generated from pool so we sending it back to the pool
        {

          //CoreCommon.SharedUtils.SocketExtension.ReceiveFromTcp(_tcpClients[internalId].Client, out byte[] Buffer);
          _tcpClients[internalId].ReceiveFromTcp(ref receivePrefix, in pool, out byte[] buffer);
          NetworkRequest[] receivedRequest = buffer.DeserializeArray<NetworkRequest>();
          //pool.Return(Buffer);

          for (int i = 0; i < receivedRequest.Length; i++)
            _genericRequestHandler.Handle(internalId, ref receivedRequest[i]);
        }
      }
      catch (Exception e)
      {
        Console.WriteLine("Exception occur in TCP: " + e);
        _disconnectionHandler.Disconnect(internalId);
      }
    }

    public void Dispose()
    {
      _tcpThread?.Abort();
    }
  }
}