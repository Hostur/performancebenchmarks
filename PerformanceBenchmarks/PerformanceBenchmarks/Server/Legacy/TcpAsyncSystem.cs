﻿using System;
using System.Buffers;
using System.Net.Sockets;
using System.Threading.Tasks;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils.Serialization;
using PerformanceBenchmarks.Logic;

namespace PerformanceBenchmarks.Server.Legacy
{
  public class StateObject
  {
    public const int BufferSize = 1024;

    public int InternalId;
    public byte[] Buffer;

    public StateObject(int internalId)
    {
      InternalId = internalId;
      Buffer = new byte[BufferSize];
    }
  }

  [CoreRegister(true)]
  public class StateObjects
  {
    public StateObject[] _obj;

    public StateObjects(INetworkSettings settings)
    {
      _obj = new StateObject[settings.ClientsCapacity];
    }

    public StateObject this[int internalId]
    {
      get => _obj[internalId];
      set => _obj[internalId] = value;
    }
  }

  [CoreRegister(true)]
  public class TcpAsyncSystem : IDisposable
  {
    private readonly ClientAssigned _clientAssigned;
    private readonly OutgoingTCPRequests _outgoingTcpRequests;
    private readonly DisconnectHandler _disconnectionHandler;
    private readonly GenericRequestHandler _genericRequestHandler;
    private readonly TCPClients _tcpClients;
    private readonly ICoreLogger _coreLogger;
    private readonly INetworkSettings _networkSettings;
    private readonly StateObjects _stateObjects;
    private bool _running;

    public TcpAsyncSystem(
      OutgoingTCPRequests outgoingTcpRequests,
      DisconnectHandler disconnectionHandler,
      GenericRequestHandler genericRequestHandler,
      ClientAssigned clientAssigned,
      TCPClients tcpClients,
      ICoreLogger coreLogger,
      INetworkSettings networkSettings, StateObjects stateObjects)
    {
      _outgoingTcpRequests = outgoingTcpRequests;
      _disconnectionHandler = disconnectionHandler;
      _genericRequestHandler = genericRequestHandler;
      _clientAssigned = clientAssigned;
      _tcpClients = tcpClients;
      _coreLogger = coreLogger;
      _networkSettings = networkSettings;
      _stateObjects = stateObjects;
    }

    public async Task Start(int serverCapacity)
    {
      _running = true;
      await ProceedClients(1, serverCapacity).ConfigureAwait(false);
      _coreLogger.Log("TCP system started.", LogType.Info);
    }

    private async Task ProceedClients(int start, int end)
    {
      ArrayPool<byte> pool = ArrayPool<byte>.Shared;
      byte[] receivePrefix = new byte[4];

      while (_running)
      {
        for (int i = start; i < end; i++)
        {
          if (_clientAssigned[i])
          {
            await HandleTcp(i, pool).ConfigureAwait(false);
          }
        }
      }
    }

    private async Task HandleTcp(int internalId, ArrayPool<byte> pool)
    {
      try
      {
        TcpClient client = _tcpClients[internalId];
        if (!client.Connected)
        {
          _disconnectionHandler.Disconnect(internalId);
          return;
        }

        if (_outgoingTcpRequests.TryDequeue(internalId, out NetworkRequest[] result))
        {
          var bytes = result.SerializeArray();
          await client.Client.SendAsync(new ReadOnlyMemory<byte>(bytes), SocketFlags.None).ConfigureAwait(false);
          //client.Client.SendTcp(ref bytes);
        }


        if (!_tcpClients[internalId].GetStream().DataAvailable)
          return;

        // result is generated from pool so we sending it back to the pool
        {
          _tcpClients[internalId].Client.BeginReceive(_stateObjects[internalId].Buffer, 0, 4, SocketFlags.None,
            new AsyncCallback(ReceivePrefixCallback), _stateObjects[internalId]);

          //CoreCommon.SharedUtils.SocketExtension.ReceiveFromTcp(_tcpClients[internalId].Client, out byte[] Buffer);
          byte[] receivePrefix = new byte[4];
          _tcpClients[internalId].ReceiveFromTcp(ref receivePrefix, in pool, out byte[] buffer);
          NetworkRequest[] receivedRequest = buffer.DeserializeArray<NetworkRequest>();
          //pool.Return(Buffer);

          for (int i = 0; i < receivedRequest.Length; i++)
            _genericRequestHandler.Handle(internalId, ref receivedRequest[i]);
        }
      }
      catch (Exception e)
      {
        Console.WriteLine("Exception occur in TCP: " + e);
        _disconnectionHandler.Disconnect(internalId);
      }
    }

    private void ReceivePrefixCallback(IAsyncResult ar)
    {
      StateObject so = (StateObject) ar.AsyncState;
      Socket socket = _tcpClients[so.InternalId].Client;
      int read = socket.EndReceive(ar);
      int messageLength = BitConverter.ToInt32(so.Buffer.AsSpan().Slice(0, 4));
      socket.BeginReceive(so.Buffer, 4, messageLength, SocketFlags.None, new AsyncCallback(ReceiveRequestCallback), so);
    }

    private void ReceiveRequestCallback(IAsyncResult ar)
    {

    }

    public void Dispose()
    {
      _running = false;
    }
  }
}