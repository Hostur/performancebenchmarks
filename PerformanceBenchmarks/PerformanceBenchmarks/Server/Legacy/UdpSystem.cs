﻿using System;
using System.Threading;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils;
using CoreCommon.SharedUtils.Serialization;
using PerformanceBenchmarks.Logic;

namespace PerformanceBenchmarks.Server.Legacy
{
  [CoreRegister(true)]
  public class UdpSystem : IDisposable
  {
    private readonly ClientAssigned _clientAssigned;
    private readonly GenericRequestHandler _genericServerRequestHandler;
    private readonly DisconnectHandler _disconnectionHandler;
    private readonly OutgoingUDPRequests _outgoingUdpRequests;
    private readonly UdpReadiness _udpReadiness;
    private readonly UDPClients _udpClients;
    private readonly UDPEndPoints _udpEndPoints;
    private readonly ICoreLogger _coreLogger;
    private Thread _udpThread;

    public UdpSystem(
      ClientAssigned clientAssigned,
      GenericRequestHandler genericServerRequestHandler,
      DisconnectHandler disconnectionHandler,
      OutgoingUDPRequests outgoingUdpRequests,
      UdpReadiness udpReadiness,
      UDPClients udpClients,
      UDPEndPoints udpEndPoints,
      ICoreLogger coreLogger)
    {
      _outgoingUdpRequests = outgoingUdpRequests;
      _disconnectionHandler = disconnectionHandler;
      _clientAssigned = clientAssigned;
      _genericServerRequestHandler = genericServerRequestHandler;
      _udpReadiness = udpReadiness;
      _udpClients = udpClients;
      _udpEndPoints = udpEndPoints;
      _coreLogger = coreLogger;
    }

    public void Start(int serverCapacity)
    {
      ProceedClients(serverCapacity);
      _coreLogger.Log("UDP system started.", LogType.Info);
    }

    private void ProceedClients(int serverCapacity)
    {
      _udpThread = new Thread(() => ProceedClients(1, serverCapacity))
      {
        IsBackground = true
      };
      _udpThread.Start();
    }

    private void ProceedClients(int start, int end)
    {
      while (true)
      {
        try
        {
          for (int i = start; i < end; i++)
          {
            if (!_clientAssigned[i]) continue;

            try
            {
              if (_udpClients[i].ReceiveDataFromUdp(ref _udpEndPoints[i], out byte[] data))
              {
                _udpReadiness[i] = true;
                var requests = data.DeserializeArray<NetworkRequest>();
                for (int k = 0; k < requests.Length; k++)
                {
                  _genericServerRequestHandler.Handle(i, ref requests[k]);
                }
              }

              if (_udpReadiness[i] && _outgoingUdpRequests.TryDequeue(i, out NetworkRequest[] result))
              {
                var toSend = result.SerializeArray();
                if (toSend.Length > 65000)
                {
                  _coreLogger.Log("Cannot send udp request bigger than 65000 bytes.", LogType.Error);
                  return; // Cannot send so big request}
                }

                _udpClients[i].SendUdp(ref _udpEndPoints[i], ref toSend);
              }
            }
            catch (Exception e)
            {
              Console.WriteLine(e);
              _disconnectionHandler.Disconnect(i);
            }
          }
        }
        catch (Exception e)
        {
          _coreLogger.Log($"Exception occur during proceeding udp clients: {e}", LogType.Error);

        }
      }
    }

    public void Dispose()
    {
      _udpThread.Abort();
    }
  }
}