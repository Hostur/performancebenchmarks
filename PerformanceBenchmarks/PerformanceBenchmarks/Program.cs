﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using CoreCommon;
using CoreCommon.DI;
using CoreCommon.SharedUtils;
using CoreCommon.SharedUtils.Requests;
using Open.Nat;
using PerformanceBenchmarks.Server.Legacy;
using PerformanceBenchmarks.Server.Pipeline;
using PerformanceBenchmarks.SharedServerTypes;


namespace PerformanceBenchmarks
{
  public class Program
  {
    private static WebServiceProvider _webServiceProvider;
    static async void OnProcessExit(object sender, EventArgs e)
    {
      _webServiceProvider?.PublishServer(new PublishServerStateRequest()
      {
        LocalIpAddress = GetLocalIPAddress(),
        State = ServerState.Close,
        Port = God.PrayFor<INetworkSettings>().DefaultTCPPort
      }).Wait();

      God.PrayFor<IList<IDisposable>>()?.Each(d => d?.Dispose());
    }

    public static void Main(string[] args)
    {
      INetworkSettings settings = God.PrayFor<INetworkSettings>();

      #region Handle capacity and port forwarding
      if (args == null || args.Length < 2)
      {
        Console.WriteLine("Clients capacity argument not found.");
        Console.WriteLine("Enter capacity argument to continue:");
        string capacity = string.Empty;
        while (string.IsNullOrEmpty(capacity) || !int.TryParse(capacity, out _))
        {
          Console.WriteLine("Enter valid number:");
          capacity = Console.ReadLine();
        }
       (settings as NetworkSettings).ClientsCapacity = int.Parse(capacity);

        Console.WriteLine("Do you want to forward ports? WARNING: For high server capacity, forwarding can take a lot of time.\nYou don't need it on virtual hosting machines.");
        string forwardPorts = string.Empty;
        while (string.IsNullOrEmpty(forwardPorts) || !bool.TryParse(forwardPorts, out _))
        {
          Console.WriteLine("Enter true or false...");
          forwardPorts = Console.ReadLine();
        }
       (settings as NetworkSettings).ForwardPorts = bool.Parse(forwardPorts);
      }
      else
      {
        if (!int.TryParse(args[0], out int cap))
        {
          Console.WriteLine("Clients capacity argument not found.");
          Console.WriteLine("Enter capacity argument to continue:");
          string capacity = string.Empty;
          while (string.IsNullOrEmpty(capacity) || !int.TryParse(capacity, out _))
          {
            capacity = Console.ReadLine();
          }

          cap = int.Parse(capacity);
        }

        if (!bool.TryParse(args[1], out bool punch))
        {
          Console.WriteLine("Do you want to forward ports? WARNING: For high server capacity, forwarding can take a lot of time.\n You don't need it on virtual hosting machines.");
          string forwardPorts = string.Empty;
          while (string.IsNullOrEmpty(forwardPorts) || !bool.TryParse(forwardPorts, out _))
          {
            forwardPorts = Console.ReadLine();
          }

          punch = bool.Parse(forwardPorts);
        }

        (settings as NetworkSettings).ClientsCapacity = cap;
        (settings as NetworkSettings).ForwardPorts = punch;
      }
      #endregion


      AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
      _webServiceProvider = new WebServiceProvider(false);
      ICoreLogger logger = God.PrayFor<ICoreLogger>();

      Task.Run(async () =>
      {
        bool initResult = await Init(_webServiceProvider, settings, logger).ConfigureAwait(false);

        if(initResult)
          logger.Log("Starting server.", LogType.Info);
        else
          logger.Log("Starting server after web service or port forwarding initialization failed.", LogType.Warning);

        SerializableTypesInitializer.Register();
        var server = God.PrayFor<LegacyServer>();
        //var server = God.PrayFor<PipelineServer>();

        server.Start();
        await ShowSpeed(logger).ConfigureAwait(false);
      });

      string command = string.Empty;
      while ((command = System.Console.ReadLine()) != "exit")
      {
        ShowRequestsAndClear(logger);
      }
    }

    private static async Task ShowSpeed(ICoreLogger logger)
    {
      while (true)
      {
        await Task.Delay(1000).ConfigureAwait(false);
        ShowRequestsAndClear(logger);
      }
    }

    private static void ShowRequestsAndClear(ICoreLogger logger)
    {
      if (RequestHandler.HANDLED_REQUESTS > 0)
      {
        logger.Log($"Handled requests: {RequestHandler.HANDLED_REQUESTS}", LogType.Info);
        RequestHandler.HANDLED_REQUESTS = 0;
      }

      if (RequestHandler.SENT_REQUESTS > 0)
      {
        logger.Log($"Sender requests: {RequestHandler.SENT_REQUESTS}", LogType.Info);
        RequestHandler.SENT_REQUESTS = 0;
      }
    }

    private static async Task<bool> Init(WebServiceProvider webServiceProvider, INetworkSettings settings, ICoreLogger coreLogger)
    {
      try
      {
        coreLogger.Log("Initializing web service.", LogType.Info);
        await webServiceProvider.Initialize().ConfigureAwait(false);
        if (!webServiceProvider.Initialized)
        {
          coreLogger.Log("Cannot initialize web service.", LogType.Error);
          return false;
        }
      }
      catch (Exception e)
      {
        coreLogger.Log($"Exception occur during web service initialization: {e}", LogType.Error);
        return false;
      }

      try
      {
        int forwardedPort = 0;
        if (settings.ForwardPorts)
        {
          coreLogger.Log("Trying to forward ports.", LogType.Info);
          await NAT.RemoveMappingOnThisIp().ConfigureAwait(false);
        
          try
          {
            forwardedPort = await NAT
              .CreateMapping(God.PrayFor<INetworkSettings>().DefaultTCPPort, Protocol.Tcp, "Benchmarks")
              .ConfigureAwait(false);

            int start = settings.DefaultTCPPort;
            int ends = start + settings.ClientsCapacity + 1;
            for (int i = start; i < ends; i++)
            {
              var tmp = await NAT
                .CreateMapping(i, Protocol.Udp, $"Benchmarks UDP ({i})")
                .ConfigureAwait(false);
              God.PrayFor<MappedUDPPorts>().AddMappedPort(tmp, i);
            }
          }
          catch
          {
            coreLogger.Log("Failed to forward tcp port.", LogType.Warning);
          }
        }

        coreLogger.Log("Trying to publish server info to web service.", LogType.Info);
        var result = await webServiceProvider.PublishServer(new PublishServerStateRequest
        {
          Port = (forwardedPort < 1) ? settings.DefaultTCPPort : forwardedPort,
          State = ServerState.Available,
          LocalIpAddress = IP.GetLocalIPAddress(),
        }).ConfigureAwait(false);

        return result.IsSuccessful && result.Value;
      }
      catch (Exception e)
      {
        coreLogger.Log($"Exception occur during initialization: {e}", LogType.Error);
        return false;
      }
    }

    private static string GetLocalIPAddress()
    {
      var host = Dns.GetHostEntry(Dns.GetHostName());
      foreach (var ip in host.AddressList)
      {
        if (ip.AddressFamily == AddressFamily.InterNetwork)
        {
          return ip.ToString();
        }
      }

      Console.WriteLine("No network adapters with an IPv4 address in the system!");
      throw new Exception();
    }
  }
}
