﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using CoreDI.Attributes;

namespace CoreDI
{
  /// <summary>
  /// Standalone (PC) registration register each class with <see cref="CoreRegisterAttribute"/> above.
  /// </summary>
  internal class StandaloneAssemblyRegistrationModule
  {
    public void Register(ContainerBuilder builder)
    {
      foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
      {
        List<Type> typesToRegister;
        try
        {
          typesToRegister = assembly.GetTypes().Where(t => t.IsDefined(typeof(CoreRegisterAttribute))).ToList();
        

          foreach (Type t in typesToRegister)
          {
            bool singletone = t.GetCustomAttribute<CoreRegisterAttribute>(true).IsSingletone;
            if (singletone)
            {
              if (t.GetInterfaces().Contains(typeof(IDisposable)))
              {
                builder.RegisterType(t)
                  .AsSelf()
                  .AsImplementedInterfaces()
                  .As<IDisposable>()
                  .Keyed<object>(t.FullName)
                  .SingleInstance();
              }
              else
              {
                builder.RegisterType(t)
                  .AsSelf()
                  .AsImplementedInterfaces()
                  .Keyed<object>(t.FullName)
                  .SingleInstance();
              }
            }
            else
            {
              if (t.GetInterfaces().Contains(typeof(IDisposable)))
              {
                builder.RegisterType(t)
                  .AsSelf()
                  .AsImplementedInterfaces()
                  .As<IDisposable>()
                  .Keyed<object>(t.FullName);
              }
              else
              {
                builder.RegisterType(t)
                  .AsSelf()
                  .AsImplementedInterfaces()
                  .Keyed<object>(t.FullName);
              }
            }
          }
        }
        catch
        {
          // Ignore types loading exception
          continue;
        }
      }
    }
  }
}
