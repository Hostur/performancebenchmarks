﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
namespace CoreDI
{
  public static class God
  {
    public enum RegistrationType
    {
      Standalone,
      Mobile
    }

    private static IContainer _container;

    private static IContainer Container => _container ?? (_container = WorldCreation(RegistrationType.Standalone));

    public static IContainer WorldCreation(RegistrationType registrationType)
    {
      try
      {
        ContainerBuilder builder = new ContainerBuilder();
        // If no assembly moded was provided than we use default standalone (PC) registration.
        if (registrationType == RegistrationType.Standalone)
          new StandaloneAssemblyRegistrationModule().Register(builder);
        else // If custom registration module provided than we register hardcoded libraries types before custom assemblies.
        {
          // Get modules types
          var assembly = GetAssemblyModules().ToList();
          List<IRegisterAssemblyModule> modules = new List<IRegisterAssemblyModule>(assembly.Count);

          // Create modules instances
          foreach (Type assemblyModule in assembly)
            modules.Add(Activator.CreateInstance(assemblyModule) as IRegisterAssemblyModule);

          // Reorder instances
          modules = modules.OrderBy(c => c.RegistrationOrder).ToList();

          // Register all of them
          foreach (IRegisterAssemblyModule registerAssemblyModule in modules)
            registerAssemblyModule.Register(builder);
        }
        
        var container = builder.Build();

        return container;
      }
      catch (Exception e)
      {
        throw new Exception("[AUTOFAC] \n" +
                            $"Exception occur during WorldCreation. \n{e}");
      }
    }

    /// <summary>
    /// Generic types resolver.
    /// </summary>
    /// <typeparam name="T">Type of registered object.</typeparam>
    /// <returns>Instance.</returns>
    public static T PrayFor<T>()
    {
      return Container.Resolve<T>();
    }

    /// <summary>
    /// Gets registered object instance by type.
    /// This function is used by internal dependency injection automatization for game objects. 
    /// For normal case use <see cref="PrayFor{T}"/>.
    /// </summary>
    /// <param name="type">Requested type.</param>
    /// <returns>Instance.</returns>
    public static object PrayFor(Type type)
    {
      return Container.ResolveNamed<object>(type.FullName);
    }

    private static IEnumerable<Type> GetAssemblyModules()
    {
      List<Type> result = new List<Type>();
      foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
      {
        Console.WriteLine(assembly.FullName);
        try
        {
          var types = assembly.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IRegisterAssemblyModule))).ToList();
          foreach (Type type in types)
          {
            result.Add(type);
          }
        }
        catch
        {
          // Ignore types loading exception
          continue;
        }
      }
      return result;
    }
  }
}
