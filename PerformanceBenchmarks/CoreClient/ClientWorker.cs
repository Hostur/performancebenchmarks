﻿using System;
using System.Threading;
using CoreClient.Collections;
using CoreClient.Networking;
using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils.Serialization;

namespace CoreClient
{
  public sealed class ClientWorker : IDisposable
  {
    private readonly Thread _networkThread;
    private readonly RequestsQueues _requestsQueues;
    private OutgoingClientRequests _outgoingClientRequests;
    private readonly GenericRequestHandler _genericRequestHandler;
    public readonly INetworkClient NetworkClient;
    private readonly ICoreLogger _coreLogger;

    private int _workerIndex;

    public ClientWorker(
      RequestsQueues requestsQueues,
      GenericRequestHandler genericRequestHandler,
      INetworkClient networkClient,
      ICoreLogger coreLogger)
    {
      _networkThread = new Thread(ProceedNetworkTransport);
      _networkThread.IsBackground = true;
      _requestsQueues = requestsQueues;
      _genericRequestHandler = genericRequestHandler;
      NetworkClient = networkClient;
      _coreLogger = coreLogger;
    }

    public void Start(int workerIndex, string tcpAddress, string privateTcpAddress, int tcpPort)
    {
      _workerIndex = workerIndex;
      _outgoingClientRequests = _requestsQueues[workerIndex];
      try
      {
        NetworkClient.Start(tcpAddress, tcpPort);
      }
      catch
      {
        NetworkClient.Start(privateTcpAddress, tcpPort);
      }

      _networkThread.Start();
    }

    private void ProceedNetworkTransport()
    {
      Console.WriteLine("Start proceeding network transport.");
      while (true)
      {
        try
        {
          // Receive tcp.
          while (NetworkClient.ReceiveDataFromTcp(out byte[] incomingBuffer))
          {
            NetworkRequest[] requests = incomingBuffer.DeserializeArray<NetworkRequest>();
            for (int i = 0; i < requests.Length; i++)
              _genericRequestHandler.Handle(_workerIndex, ref requests[i]);
          }

          // Receive udp.
          while (NetworkClient.ReceiveDataFromUdp(out byte[] incomingBuffer))
          {
            var udp = incomingBuffer.DeserializeArray<NetworkRequest>();
            for (int i = 0; i < udp.Length; i++)
              _genericRequestHandler.Handle(_workerIndex, ref udp[i]);
          }

          if (_outgoingClientRequests.TryDequeueTcp(out NetworkRequest[] outgoingTcp))
            NetworkClient.SendThroughTcp(outgoingTcp.SerializeArray());

          if (_outgoingClientRequests.TryDequeueUdp(out NetworkRequest[] outgoingUdp))
            NetworkClient.SendThroughUdp(outgoingUdp.SerializeArray());
        }
        catch (Exception e)
        {
          _coreLogger.Log($"WaitForMessages(tcp) exception occur: {e}", LogType.Error);
        }
      }
    }

    public void Dispose()
    {
      _networkThread?.Abort();
      NetworkClient?.Dispose();
    }
  }
}
