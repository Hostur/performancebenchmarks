﻿using System;
using System.Collections.Generic;
using Autofac;
using CoreClient.Collections;
using CoreClient.Networking;
using CoreCommon;
using CoreCommon.DI;

namespace CoreClient
{
  public sealed class MobileAssemblyRegisterModule : IRegisterAssemblyModule
  {
    public int RegistrationOrder => 1;
    public void Register(ContainerBuilder builder)
    {
      builder.Register(c => new ClientNetworkBenchmarkTestSettings())
        .As<ClientNetworkBenchmarkTestSettings>()
        .Keyed<object>(typeof(ClientNetworkBenchmarkTestSettings).FullName)
        .SingleInstance();

      builder.Register(c => new ClientNetworkSettings())
        .As<ClientNetworkSettings>()
        .As<INetworkSettings>()
        .Keyed<object>(typeof(ClientNetworkSettings).FullName)
        .SingleInstance();

      builder.Register(c => new RequestsQueues(c.Resolve<INetworkSettings>()))
        .As<RequestsQueues>()
        .Keyed<object>(typeof(RequestsQueues).FullName)
        .SingleInstance();

      builder.Register(c => new NetworkClient())
        .As<NetworkClient>()
        .As<INetworkClient>()
        .InstancePerDependency();

      RegisterRequestHandlers(builder);

      builder.Register(c => new RequestsQueues(c.Resolve<INetworkSettings>()))
        .As<RequestsQueues>()
        .Keyed<object>(typeof(RequestsQueues).FullName)
        .SingleInstance();

      builder.Register(c => new ClientWorkers(
          c.Resolve<INetworkSettings>(),
          c.Resolve<RequestsQueues>(),
          c.Resolve<GenericRequestHandler>(),
          c.Resolve<ICoreLogger>()))
        .As<ClientWorkers>()
        .Keyed<object>(typeof(ClientWorkers).FullName)
        .SingleInstance();

      builder.Register(c => new AsyncClientWorkers(
          c.Resolve<INetworkSettings>(),
          c.Resolve<RequestsQueues>(),
          c.Resolve<GenericRequestHandler>(),
          c.Resolve<ICoreLogger>()))
        .As<AsyncClientWorkers>()
        .Keyed<object>(typeof(AsyncClientWorkers).FullName)
        .SingleInstance();

      builder.Register(c => new ClientsRunner(
          c.Resolve<ICoreLogger>(),
          c.Resolve<INetworkSettings>(),
          c.Resolve<ClientNetworkBenchmarkTestSettings>(),
          c.Resolve<RequestsQueues>(),
          c.Resolve<ClientWorkers>()))
        .As<ClientsRunner>()
        .As<IDisposable>()
        .Keyed<object>(typeof(ClientsRunner).FullName)
        .SingleInstance();

      builder.Register(c => new AsyncClientsRunner(
          c.Resolve<ICoreLogger>(),
          c.Resolve<INetworkSettings>(),
          c.Resolve<ClientNetworkBenchmarkTestSettings>(),
          c.Resolve<RequestsQueues>(),
          c.Resolve<AsyncClientWorkers>()))
        .As<AsyncClientsRunner>()
        .As<IDisposable>()
        .Keyed<object>(typeof(AsyncClientsRunner).FullName)
        .SingleInstance();
    }

    private void RegisterRequestHandlers(ContainerBuilder builder)
    {
      builder.Register(c => new Vector3RequestHandler())
        .As<Vector3RequestHandler>()
        .As<IRequestHandler>()
        .SingleInstance();

      builder.Register(c => new ConfirmationRequestHandler(
          c.Resolve<RequestsQueues>(), 
          c.Resolve<ICoreLogger>(), 
          c.Resolve<INetworkSettings>()))
        .As<ConfirmationRequestHandler>()
        .As<IRequestHandler>()
        .SingleInstance();

      builder.Register(c => new RequestHandlersDictionary(c.Resolve<IList<IRequestHandler>>()))
        .As<RequestHandlersDictionary>()
        .SingleInstance();

      builder.Register(c => new GenericRequestHandler(c.Resolve<RequestHandlersDictionary>(), c.Resolve<ICoreLogger>()))
        .As<GenericRequestHandler>()
        .SingleInstance();
    }
  }
}
