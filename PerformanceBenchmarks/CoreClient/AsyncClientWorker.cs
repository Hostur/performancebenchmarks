﻿using System;
using System.Threading.Tasks;
using CoreClient.Collections;
using CoreClient.Networking;
using CoreCommon;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils.Serialization;

namespace CoreClient
{
  public sealed class AsyncClientWorker
  {
    private readonly RequestsQueues _requestsQueues;
    private OutgoingClientRequests _outgoingClientRequests;
    private readonly GenericRequestHandler _genericRequestHandler;
    public readonly INetworkClient NetworkClient;
    private readonly ICoreLogger _coreLogger;

    private int _workerIndex;

    public AsyncClientWorker(
      RequestsQueues requestsQueues,
      GenericRequestHandler genericRequestHandler,
      INetworkClient networkClient,
      ICoreLogger coreLogger)
    {
      _requestsQueues = requestsQueues;
      _genericRequestHandler = genericRequestHandler;
      NetworkClient = networkClient;
      _coreLogger = coreLogger;
    }

    public void Start(int workerIndex, string tcpAddress, string privateTcpAddress, int tcpPort)
    {
      _workerIndex = workerIndex;
      _outgoingClientRequests = _requestsQueues[workerIndex];
      try
      {
        NetworkClient.Start(tcpAddress, tcpPort);
      }
      catch
      {
        NetworkClient.Start(privateTcpAddress, tcpPort);
      }
    }

    public async Task Update()
    {
      try
      {
        var receiveTcp = await NetworkClient.ReceiveAsyncFromTcp().ConfigureAwait(false);
        if (receiveTcp != null)
        {
          NetworkRequest[] requests = receiveTcp.DeserializeArray<NetworkRequest>();
          for (int i = 0; i < requests.Length; i++)
            _genericRequestHandler.Handle(_workerIndex, ref requests[i]);
        }

        var receiveUdp = await NetworkClient.ReceiveDataFromUdpAsync().ConfigureAwait(false);
        if (receiveUdp != null)
        {
          NetworkRequest[] requests = receiveUdp.DeserializeArray<NetworkRequest>();
          for (int i = 0; i < requests.Length; i++)
            _genericRequestHandler.Handle(_workerIndex, ref requests[i]);
        }

        if (_outgoingClientRequests.TryDequeueTcp(out NetworkRequest[] outgoingTcp))
          await NetworkClient.SendAsyncThroughTcp(outgoingTcp.SerializeArray()).ConfigureAwait(false);

        if (_outgoingClientRequests.TryDequeueUdp(out NetworkRequest[] outgoingUdp))
          await NetworkClient.SendThroughUdpAsync(outgoingUdp.SerializeArray()).ConfigureAwait(false);
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }
    }
  }
}
