﻿using System;
using System.Threading.Tasks;
using CoreClient.Collections;
using CoreClient.Networking;
using CoreCommon;
using CoreCommon.DI;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils;
using CoreCommon.SharedUtils.Serialization;

namespace CoreClient
{
  [CoreRegister(true)]
  public sealed class ClientsRunner : IDisposable
  {
    private readonly ICoreLogger _coreLogger;
    private readonly ClientNetworkBenchmarkTestSettings _benchmarkTestSettings;
    private INetworkSettings _networkSettings;
    private readonly ClientWorkers _clientWorkers;
    private readonly RequestsQueues _requestsQueues;
    private bool _running;

    public ClientsRunner(
      ICoreLogger coreLogger,
      INetworkSettings networkSettings,
      ClientNetworkBenchmarkTestSettings benchmarkTestSettings,
      RequestsQueues requestsQueues, ClientWorkers clientWorkers)
    {
      _coreLogger = coreLogger;
      _networkSettings = networkSettings;
      _benchmarkTestSettings = benchmarkTestSettings;
      _requestsQueues = requestsQueues;
      _clientWorkers = clientWorkers;
    }

    private async Task PushRequestsToQueue()
    {
      _coreLogger.Log("Start to push requests to queues.", LogType.Info);
      var request = new NetworkRequest(Vector3Request.REQUEST_IDENTIFIER, new Vector3Request(new SerializableVector3(2567, -21.3f, 185.333f)).Serialize());
      while (_running)
      {
        for (int i = 0; i < _networkSettings.ClientsCapacity; i++)
        {
          for (int k = 0; k < _benchmarkTestSettings.AmountOfRequestsPerTick; k++)
          {
            _requestsQueues[i].EnqueueTcp(ref request);
            _requestsQueues[i].EnqueueUdp(ref request);
          }
        }

        await Task.Delay(_benchmarkTestSettings.PushingRequestsIntervalInMilliseconds).ConfigureAwait(false);
      }
    }

    public async Task Run(string webServiceAddress)
    {
      WebServiceProvider webServiceProvider = new WebServiceProvider(false, webServiceAddress);
      await webServiceProvider.Initialize().ConfigureAwait(false);
      if (!webServiceProvider.Initialized)
      {
        _coreLogger.Log("Failed to initialize web service.", LogType.Error);
        return;
      }

      var servers = await webServiceProvider.GetServer().ConfigureAwait(false);
      if (!servers.IsSuccessful)
      {
        _coreLogger.Log("Not servers found.", LogType.Error);
        return;
      }

      Run(servers.Value.Address, servers.Value.PrivateAddress, servers.Value.Port);
    }

    public void Run(string serverAddress, string privateServerAddress, int serverPort)
    {
      _coreLogger.Log($"Starting test with {_networkSettings.ClientsCapacity} clients.", LogType.Info);
      _coreLogger.Log(
        $"Each client will send" +
        $" {_benchmarkTestSettings.AmountOfRequestsPerTick} " +
        $"UDP and " +
        $"{_benchmarkTestSettings.AmountOfRequestsPerTick} tcp requests per " +
        $"{_benchmarkTestSettings.PushingRequestsIntervalInMilliseconds} ms.", LogType.Info);


      for (int i = 0; i < _networkSettings.ClientsCapacity; i++)
      {
        _clientWorkers[i].Start(i, serverAddress, privateServerAddress, serverPort);
        _coreLogger.Log($"{i} client started.", LogType.Info);
      }

      _running = true;
      Task.Run(async () =>
      {
        await Task.Delay(_benchmarkTestSettings.DelayBeforePushingRequests).ConfigureAwait(false);
        await PushRequestsToQueue().ConfigureAwait(false);
      }).ConfigureAwait(false);
    }

    public void Dispose()
    {
      _running = false;
      for (int i = 0; i < _networkSettings.ClientsCapacity; i++)
      {
        _clientWorkers[i]?.Dispose();
      }
    }
  }
}
