﻿using CoreCommon;
using CoreCommon.DI.Attributes;

namespace CoreClient
{
  [CoreRegister(true)]
  public sealed class ClientNetworkSettings : INetworkSettings
  {
    public int ClientsCapacity => 10;
    public bool ForwardPorts => false;
    public int DefaultTCPPort => 6331;
    public bool Async => false;
  }

  [CoreRegister(true)]
  public sealed class ClientNetworkBenchmarkTestSettings
  {
    /// <summary>
    /// How often we are pushing requests to queues.
    /// </summary>
    public int PushingRequestsIntervalInMilliseconds = 33;

    /// <summary>
    /// How many requests we are pushing to UDP and TCP queue each time (defined as PushingRequestsIntervalInMilliseconds).
    /// </summary>
    public int AmountOfRequestsPerTick = 10;

    /// <summary>
    /// For how long we wait from the connecting new clients until we start to push requests to queues.
    /// </summary>
    public int DelayBeforePushingRequests => 3000;

    /// <summary>
    /// How often <see cref="AsyncClientsRunner"/> update <see cref="AsyncClientWorker"/>s.
    /// </summary>
    public int AsyncWorkerTickTime => 33;
  }
}
