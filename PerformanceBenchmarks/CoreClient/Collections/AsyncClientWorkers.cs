﻿using System;
using CoreClient.Networking;
using CoreCommon;
using CoreCommon.DI;
using CoreCommon.DI.Attributes;

namespace CoreClient.Collections
{
  [CoreRegister(true)]
  public sealed class AsyncClientWorkers : IDisposable
  {
    private readonly AsyncClientWorker[] _workers;

    public int Length => _workers.Length;

    public AsyncClientWorkers(INetworkSettings settings, RequestsQueues requestsQueues, GenericRequestHandler genericRequestHandler, ICoreLogger logger)
    {
      _workers = new AsyncClientWorker[settings.ClientsCapacity];
      for (int i = 0; i < settings.ClientsCapacity; i++)
      {
        _workers[i] = new AsyncClientWorker(requestsQueues, genericRequestHandler, God.PrayFor<INetworkClient>(), logger);
      }
    }

    public AsyncClientWorker this[int internalId] => _workers[internalId];

    public void Dispose()
    {
      _workers?.Each(w => w.NetworkClient?.Dispose());
    }
  }
}
