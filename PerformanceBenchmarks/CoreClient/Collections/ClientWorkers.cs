﻿using System;
using CoreClient.Networking;
using CoreCommon;
using CoreCommon.DI;
using CoreCommon.DI.Attributes;

namespace CoreClient.Collections
{
  [CoreRegister(true)]
  public sealed class ClientWorkers : IDisposable
  {
    private readonly ClientWorker[] _workers;

    public int Length => _workers.Length;

    public ClientWorkers(INetworkSettings settings, RequestsQueues requestsQueues, GenericRequestHandler genericRequestHandler, ICoreLogger logger)
    {
      _workers = new ClientWorker[settings.ClientsCapacity];
      for (int i = 0; i < settings.ClientsCapacity; i++)
      {
        _workers[i] = new ClientWorker(requestsQueues, genericRequestHandler, God.PrayFor<INetworkClient>(),  logger);
      }
    }

    public ClientWorker this[int internalId] => _workers[internalId];

    public void Dispose()
    {
      _workers?.Each(w => w?.Dispose());
    }
  }
}
