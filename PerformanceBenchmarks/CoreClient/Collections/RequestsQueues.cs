﻿using CoreCommon;
using CoreCommon.DI.Attributes;

namespace CoreClient.Collections
{
  [CoreRegister(true)]
  public sealed class RequestsQueues
  {
    private readonly OutgoingClientRequests[] _queues;

    public RequestsQueues(INetworkSettings settings)
    {
      _queues = new OutgoingClientRequests[settings.ClientsCapacity];
      for(int i = 0; i < settings.ClientsCapacity; i++)
        _queues[i] = new OutgoingClientRequests(50);
    }

    public OutgoingClientRequests this[int internalId] => _queues[internalId];
  }
}
