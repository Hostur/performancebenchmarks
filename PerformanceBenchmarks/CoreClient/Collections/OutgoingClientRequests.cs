﻿using CoreCommon;
using CoreCommon.Serializable;

namespace CoreClient.Collections
{
  public class OutgoingClientRequests
  {
    private NetworkRequestsQueue _tcpQueue;
    private NetworkRequestsQueue _udpQueue;

    public OutgoingClientRequests(int capacity)
    {
      _tcpQueue = new NetworkRequestsQueue(capacity);
      _udpQueue = new NetworkRequestsQueue(capacity);
    }

    public void EnqueueTcp(ref NetworkRequest req) => _tcpQueue.Enqueue(ref req);
    public void EnqueueUdp(ref NetworkRequest req) => _udpQueue.Enqueue(ref req);

    public bool TryDequeueTcp(out NetworkRequest[] requests) => _tcpQueue.TryDequeue(out requests);
    public bool TryDequeueUdp(out NetworkRequest[] requests) => _udpQueue.TryDequeue(out requests);
  }
}
