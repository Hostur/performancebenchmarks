﻿using CoreCommon;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;

namespace CoreClient
{
  [CoreRegister(true)]
  public sealed class Vector3RequestHandler : RequestHandler
  {
    public override byte RequestId => Vector3Request.REQUEST_IDENTIFIER;
    public override void Handle(int internalId, ref NetworkRequest request)
    {
      ++HANDLED_REQUESTS;
    }
  }
}
