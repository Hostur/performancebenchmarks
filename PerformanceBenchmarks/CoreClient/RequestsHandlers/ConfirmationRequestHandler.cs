﻿using CoreClient.Collections;
using CoreCommon;
using CoreCommon.DI;
using CoreCommon.DI.Attributes;
using CoreCommon.Serializable;
using CoreCommon.SharedUtils.Serialization;

namespace CoreClient
{
  [CoreRegister(true)]
  public sealed class ConfirmationRequestHandler : RequestHandler
  {
    private readonly RequestsQueues _requestsQueues;
    private readonly ICoreLogger _coreLogger;
    private readonly INetworkSettings _settings;
    public ConfirmationRequestHandler(RequestsQueues requestsQueues,
      ICoreLogger coreLogger,
      INetworkSettings settings)
    {
      _requestsQueues = requestsQueues;
      _coreLogger = coreLogger;
      _settings = settings;
    }

    public override byte RequestId => ConfirmationRequest.REQUEST_IDENTIFIER;

    public override void Handle(int internalId, ref NetworkRequest request)
    {
      ConfirmationRequest req = request.Data.Deserialize<ConfirmationRequest>();

      if (req.ConfirmationState == ConfirmationState.RequestConfirmation)
      {
        NetworkRequest confirmation = new NetworkRequest(ConfirmationRequest.REQUEST_IDENTIFIER, new ConfirmationRequest(string.Empty).Serialize());
        _requestsQueues[internalId].EnqueueTcp(ref confirmation);
        return;
      }

      if (req.ConfirmationState == ConfirmationState.ConfirmationAccepted)
      {
        if(_settings.Async)
          God.PrayFor<AsyncClientWorkers>()[internalId].NetworkClient.SetUpUdpListener(req.UdpPort);
        else
          God.PrayFor<ClientWorkers>()[internalId].NetworkClient.SetUpUdpListener(req.UdpPort);
        _coreLogger.Log("Client confirmed and udp client created.", LogType.Info);
        return;
      }
      
      if(req.ConfirmationState == ConfirmationState.ConfirmationFailed)
        _coreLogger.Log("Confirmation failed.", LogType.Error);
    }
  }
}
