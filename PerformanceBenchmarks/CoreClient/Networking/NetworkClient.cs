﻿using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using CoreCommon.DI.Attributes;
using CoreCommon.SharedUtils;

namespace CoreClient.Networking
{
  [CoreRegister(false)]
  internal class NetworkClient : INetworkClient
  {
    private TcpClient _tcpClient;
    private string _tcpAddress;
    private NetworkStream _networkStream;
    private UdpClient _udpClient;
    private IPEndPoint _ipEndPoint;

    public void Start(string tcpAddress, int tcpPort)
    {
      _tcpAddress = tcpAddress;
      _tcpClient = new TcpClient(tcpAddress, tcpPort) { SendTimeout = 2000, NoDelay = true };
      _networkStream = _tcpClient.GetStream();
      _ipEndPoint = _tcpClient.Client.RemoteEndPoint as IPEndPoint;
    }

    private bool IsStreamAvailable() => _tcpClient.Connected && _networkStream.DataAvailable;

    public void Dispose()
    {
      _tcpClient?.Dispose();
      _networkStream?.Dispose();
      _udpClient?.Dispose();
    }

    public int UdpPort => _ipEndPoint.Port;
    public bool ReceiveDataFromTcp(out byte[] result)
    {
      if (!IsStreamAvailable())
      {
        result = null;
        return false;
      }

      result = _tcpClient.Client.ReceiveFromTcp();
      return result?.Length > 0;
    }

    public async Task<byte[]> ReceiveAsyncFromTcp() => await _tcpClient.Client.ReceiveAsyncFromTcp().ConfigureAwait(false);

    public void SendThroughTcp(byte[] data) => _tcpClient.Client.SendTcp(ref data);
    public async Task<bool> SendAsyncThroughTcp(byte[] data) => await _tcpClient.Client.SendAsync(data).ConfigureAwait(false);

    public bool ReceiveDataFromUdp(out byte[] result) => _udpClient.ReceiveDataFromUdp(ref _ipEndPoint, out result);

    public async Task<byte[]> ReceiveDataFromUdpAsync() => await _udpClient.ReceiveDataFromUdpAsync().ConfigureAwait(false);

    public void SendThroughUdp(byte[] data) => _udpClient?.Send(data, data.Length);

    public async Task<bool> SendThroughUdpAsync(byte[] data) => await _udpClient.SendAsyncUdp(data).ConfigureAwait(false);
    public void SetUpUdpListener(int port)
    {
      _ipEndPoint.Port = port;
      _udpClient = new UdpClient();
      _udpClient.Client.ReceiveBufferSize = 64000;
      _udpClient.Client.SendBufferSize = 64000;
      _udpClient.Connect(_tcpAddress, port);
      _udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 200);
    }
  }
}
