﻿using System;
using System.Threading.Tasks;

namespace CoreClient.Networking
{
  public interface INetworkClient : IDisposable
  {
    void Start(string tcpAddress, int tcpPort);
    int UdpPort { get; }
    bool ReceiveDataFromTcp(out byte[] result);
    Task<byte[]> ReceiveAsyncFromTcp();
    bool ReceiveDataFromUdp(out byte[] result);
    Task<byte[]> ReceiveDataFromUdpAsync();
    void SendThroughTcp(byte[] data);
    Task<bool> SendAsyncThroughTcp(byte[] data);
    void SendThroughUdp(byte[] data);
    Task<bool> SendThroughUdpAsync(byte[] data);
    void SetUpUdpListener(int port);
  }
}
