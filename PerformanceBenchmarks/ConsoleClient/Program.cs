﻿using System;
using System.Threading.Tasks;
using CoreClient;
using CoreCommon;
using CoreCommon.DI;
using CoreCommon.DI.Attributes;

namespace ConsoleClient
{
  class Program
  {
    private static ClientsRunner _asyncClientsRunner;
    async static Task Main(string[] args)
    {
      SerializableTypesInitializer.Register();
      _asyncClientsRunner = God.PrayFor<ClientsRunner>();
      await _asyncClientsRunner.Run("http://www.space-smuggler.com/").ConfigureAwait(false);

      Console.WriteLine("Started..");
      Console.ReadLine();
    }
  }

  [CoreRegister(true)]
  public sealed class ConsoleLogger : ICoreLogger
  {
    private ConsoleColor GetColor(LogType logType)
    {
      switch (logType)
      {
        case LogType.Debug:
          return ConsoleColor.White;
        case LogType.Info:
          return ConsoleColor.Blue;
        case LogType.Warning:
          return ConsoleColor.Yellow;
        default:
          return ConsoleColor.Red;
      }
    }
    public void Log(string log, LogType logType)
    {
      Console.ForegroundColor = GetColor(logType);
      Console.WriteLine(log);
    }
  }
}
